package com.company.common.model.exceptions;

/**
 * Class responsible of handle system exceptions.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
public class SystemException extends Exception {
    
    /**
     * Constructor.
     * @param cause cause the error. 
     */
    public SystemException(Throwable cause) {
        super(cause);
    }
    
    /**
     * Constructor.
     * @param message error message.
     * @param cause cause the error. 
     */
    public SystemException(String message, Throwable cause) {
        super(message, cause);
    }
    
    /**
     * Constructor.
     * @param message error message.
     */
    public SystemException(String message) {
        super(message);
    }
    
}
