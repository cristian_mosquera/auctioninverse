

package com.company.common.model.businessobject;

import java.util.Date;

/**
 * Interface responsible of define contract for a object value
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
public interface IObjectValue {

    void setCreatedDate(Date createdDate); 
    
    void setUpdatedDate(Date updatedDate); 
}
