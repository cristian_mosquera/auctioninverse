package com.company.common.model.exceptions;

/**
 * Class responsible of handle validation exceptions.
 *
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
public class ValidationException extends Exception {

    /**
     * Constructor.
     *
     * @param cause cause the error.
     */
    public ValidationException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructor.
     *
     * @param message error message.
     * @param cause cause the error.
     */
    public ValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructor.
     *
     * @param message error message.
     */
    public ValidationException(String message) {
        super(message);
    }

}
