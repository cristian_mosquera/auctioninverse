package com.company.common.model.exceptions;

/**
 * Class responsible of handle system runtime exceptions.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
public class SystemRunTimeException extends RuntimeException {

    /**
     * Constructor.
     *
     * @param cause cause the error.
     */
    public SystemRunTimeException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructor.
     *
     * @param message error message.
     * @param cause cause the error.
     */
    public SystemRunTimeException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructor.
     *
     * @param message error message.
     */
    public SystemRunTimeException(String message) {
        super(message);
    }

}
