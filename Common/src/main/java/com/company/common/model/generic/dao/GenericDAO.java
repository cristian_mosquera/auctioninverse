
package com.company.common.model.generic.dao;

import com.company.common.model.exceptions.SystemException;
import java.util.List;
import org.hibernate.SessionFactory;


/**
 * Class that define contract for the general operations  on SGBD.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 * @param <K> Entity relacionared.
 * @param <V> Exception relacionared.
 */
public interface GenericDAO <K, V extends SystemException>  {
     
    /**
     * Method responsible of save entity.
     * @param entity entity to save.
     * @throws V any system error.
     */
    void save (K entity) throws V;
    
    /**
     * Method responsible of update entity.
     * @param entity entity to update.
     * @throws V any system error.
     */
    void update (K entity) throws V;
    
    /**
     * Method responsible of remove entity.
     * @param entity entity to save.
     * @throws V any system error.
     */
    void remove (K entity) throws V;
    
    /**
     * Method responsible of  find all data of the entity.
     * @return list with all data of the entity.
     * @param clazz class you want load.
     * @throws V any system error.
     */
    List <K> findAll (Class<K> clazz) throws V;
    
    /**
     * Method responsible of  find a data of the entity.
     * @param entityId id for entity.
     * @return entity loaded.
     * @param clazz class you want load.
     * @throws V any system error.
     */
    K find (Number entityId, Class<K> clazz) throws V;
    
    /**
     * Change session factory.
     * @param sessionFactory  session factory.
     * 
     */
    void setSessionFactory(SessionFactory sessionFactory);
}
