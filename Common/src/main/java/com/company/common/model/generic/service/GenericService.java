
package com.company.common.model.generic.service;
import com.company.common.model.exceptions.SystemException;
import com.company.common.model.exceptions.ValidationException;
import com.company.common.model.generic.dao.GenericDAO;
import java.util.List;

/**
 * Service that define contract for the general operations  on SGBD.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 * @param <K> Entity relacionared.
 * @param <V> System exception relacionared.
 * @param <E> Validation exception relacionared.
 */
public interface GenericService <K, V extends SystemException, E extends ValidationException>  {
     
    /**
     * Method responsible of save entity.
     * @param entity entity to save.
     * @throws V any system error.
     * @throws E any validation error.
     */
    void save (K entity) throws V, E;
    
    /**
     * Method responsible of update entity.
     * @param entity entity to update.
     * @throws V any system error.
     * @throws E any validation error.
     */
    void update (K entity) throws V, E;
    
    /**
     * Method responsible of remove entity.
     * @param entity entity to save.
     * @throws V any system error.
     * @throws E any validation error.
     */
    void remove (K entity) throws V, E;
    
    /**
     * Method responsible of  find all data of the entity.
     * @return list with all data of the entity.
     * @param clazz class you want load.
     * @throws V any system error.
     * @throws E any validation error.
     */
    List <K> findAll (Class<K> clazz) throws V, E;
    
    /**
     * Method responsible of  find a data of the entity.
     * @param entityId id for entity.
     * @return entity loaded.
     * @param clazz class you want load.
     * @throws V any system error.
     * @throws E any validation error.
     */
    K find (Number entityId, Class<K> clazz) throws V, E;
    /**
     * Change generic DAO.
     * @param genericDAO generic DAO.
     * 
     */
    void setGenericDao(GenericDAO genericDAO);
}
