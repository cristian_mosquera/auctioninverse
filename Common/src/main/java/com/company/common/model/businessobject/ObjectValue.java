package com.company.common.model.businessobject;

import java.io.Serializable;
import java.util.Date;

/**
 * General class for Object value.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Mosquera</a>
 */
public abstract class ObjectValue implements Serializable{
    
    public abstract Number getId(); 
    
    
}
