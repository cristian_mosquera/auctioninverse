package com.company.common.model.generic.dao.impl;

import com.company.common.model.exceptions.SystemException;
import com.company.common.model.generic.dao.GenericDAO;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.HibernateTemplate;

/**
 * Class that implement contract for the general operations on SGBD.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 * @param <K>  Entity relacionared.
 */
public class GenericDAOImpl <K, V extends SystemException> implements GenericDAO<K, V> {

    private HibernateTemplate hibernateTemplate;
    
    
    @Override
    public void save(K entity) throws V {
        hibernateTemplate.save(entity); 
    }

    @Override
    public void update(K entity) throws V {
        hibernateTemplate.update(entity);
    }

    @Override
    public void remove(K entity) throws V {
        hibernateTemplate.delete(entity);
    }

    @Override
    public List<K> findAll(Class<K>clazz) throws V {
        return (List<K>) hibernateTemplate.find("from " + clazz.getName());
    }

    @Override
    public K find(Number entityId, Class<K> clazz) throws V {
        return (K) hibernateTemplate.get(clazz, entityId);
    }

    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        hibernateTemplate = new HibernateTemplate(sessionFactory);
        
    }

    public HibernateTemplate getHibernateTemplate() {
        return hibernateTemplate;
    }

    public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
        this.hibernateTemplate = hibernateTemplate;
    }
     
}
