
package com.company.common.model.generic.service.impl;



import com.company.common.model.businessobject.IObjectValue;
import com.company.common.model.exceptions.SystemException;
import com.company.common.model.exceptions.SystemRunTimeException;
import com.company.common.model.exceptions.ValidationException;
import com.company.common.model.generic.dao.GenericDAO;
import com.company.common.model.generic.service.GenericService;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service that implement the general operations  on SGBD.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 * @param <K> Entity relacionared.
 */

public abstract class GenericServiceImpl<K, V extends 
                        SystemException, E extends ValidationException > 
                                                           implements GenericService<K, V, E>{
    protected Logger logger = Logger.getLogger(getClass());
    
    private GenericDAO genericDAO;
    
    @Autowired
    protected SessionFactory sessionFactory;
    
    @Transactional
    @Override
    public void save(K entity) throws E, V {
        try {
            if (entity instanceof IObjectValue) {
                ((IObjectValue) entity).setCreatedDate(new Date());
                ((IObjectValue) entity).setUpdatedDate(new Date());
            }
            genericDAO.save(entity); 
        } catch (Exception e) {
            V exception = null;
            try {
                exception = (V) SystemException.class.
                    getDeclaredConstructor(String.class, Throwable.class).
                    newInstance(e, e.getMessage());
                logger.error(e.getMessage(), e);
            } catch (Exception ex) {
                logger.error(ex.getMessage() + "\n" + e.getMessage(), ex);
                throw  new SystemRunTimeException(ex.getMessage(), ex);
            }
           throw exception;
        }
        
    }
    
    @Transactional
    @Override
    public void update(K entity) throws E, V {
        try {
            if (entity instanceof IObjectValue) {
                ((IObjectValue) entity).setUpdatedDate(new Date());
            }
            genericDAO.update(entity); 
        } catch (Exception e) {
            V exception = null;
            try {
                exception = (V) SystemException.class.
                    getDeclaredConstructor(String.class, Throwable.class).
                    newInstance(e, e.getMessage());
                logger.error(e.getMessage(), e);
            } catch (Exception ex) {
                logger.error(ex.getMessage(), ex);
                throw  new SystemRunTimeException(ex.getMessage(), ex);
            }
           throw exception;
        }
    }
    
    @Transactional
    @Override
    public void remove(K entity) throws E, V {
        try {
            genericDAO.remove(entity); 
        } catch (Exception e) {
            V exception = null;
            try {
                exception = (V) SystemException.class.
                    getDeclaredConstructor(String.class, Throwable.class).
                    newInstance(e, e.getMessage());
                logger.error(e.getMessage(), e);
            } catch (Exception ex) {
                logger.error(ex.getMessage(), ex);
                throw  new SystemRunTimeException(ex.getMessage(), ex);
            }
           throw exception;
        }
    }

    @Override
    public List<K> findAll(Class<K> clazz) throws E, V {
        try {
            return (List<K>) genericDAO.findAll(clazz);
        } catch (Exception e) {
            V exception = null;
            try {
                exception = (V) SystemException.class.
                    getDeclaredConstructor(String.class, Throwable.class).
                    newInstance(e, e.getMessage());
                logger.error(e.getMessage(), e);
            } catch (Exception ex) {
                logger.error(ex.getMessage(), ex);
                throw  new SystemRunTimeException(ex.getMessage(), ex);
            }
           throw exception;
        }
        
    }

    @Override
    public K find(Number entityId, Class<K> clazz) throws E, V {
        try {
            return (K) genericDAO.find(entityId, clazz);
        } catch (Exception e) {
            V exception = null;
            try {
                exception = (V) SystemException.class.
                    getDeclaredConstructor(String.class, Throwable.class).
                    newInstance(e, e.getMessage());
                logger.error(e.getMessage(), e);
            } catch (Exception ex) {
                logger.error(ex.getMessage(), ex);
                throw  new SystemRunTimeException(ex.getMessage(), ex);
            }
           throw exception;
        }
    }

    @Override
    public void setGenericDao(GenericDAO genericDAO) {
        this.genericDAO = genericDAO;
    }
}
