
import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import com.company.auctioninverse.model.exceptions.AuctionInverseValidationException;
import com.company.auctioninverse.model.service.GenericService;
import com.company.common.model.businessobject.ObjectValue;
import java.util.ArrayList;
import java.util.List;
import junit.framework.Assert;
import org.junit.After;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;


/**
 * Generic class for test.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Mosquera</a>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
@ContextConfiguration(locations={"classpath:/applicationContext-test.xml"})
//@TransactionConfiguration(defaultRollback=true)
@Ignore
public abstract class GenericTest <K extends ObjectValue> {
    private List<K> objectsGenerated = new ArrayList<K>();
    
    private GenericService<K> genericService;
    
    /**
     * Test of method save.
     * @throws AuctionInverseValidationException any error validation.
     * @throws AuctionInverseSystemException  any ystem error.
     */
    public void testSave() throws AuctionInverseValidationException, AuctionInverseSystemException {
        K object = generateUniqueObject(Boolean.FALSE);
        genericService.save(object);
        K objectFind = genericService.find(object.getId(), (Class<K>) object.getClass());
        Assert.assertEquals(object.getId(), objectFind.getId());
        objectsGenerated.add(object);
    }
    
    /**
     * Test method find.
     * @throws AuctionInverseValidationException any validation error.
     * @throws AuctionInverseSystemException any system error.
     */
    public void testFind() throws AuctionInverseValidationException, AuctionInverseSystemException {
        K object = generateUniqueObject(Boolean.FALSE);
        genericService.save(object);
        K objectFind = genericService.find(object.getId(), (Class<K>) object.getClass());
        Assert.assertEquals(object.getId(), objectFind.getId());
        objectsGenerated.add(object);
    }
    
    /**
     * Test method remove.
     * @throws AuctionInverseValidationException any validation error.
     * @throws AuctionInverseSystemException any system error.
     */
    public void testRemove() throws AuctionInverseValidationException, 
                                                            AuctionInverseSystemException {
        
        
    }
    
    /**
     * Test method find.
     * @throws AuctionInverseValidationException any validation error.
     * @throws AuctionInverseSystemException any system error.
     */
    public void testFindAll() throws AuctionInverseValidationException, 
                                                AuctionInverseSystemException {
        K object = generateUniqueObject(Boolean.FALSE);
        K object2 = generateUniqueObject(Boolean.FALSE);
        genericService.save(object);
        genericService.save(object2);
        List<K> objectsFind = genericService.findAll((Class<K>) object.getClass());
        Assert.assertTrue(objectsFind.contains(object));
        Assert.assertTrue(objectsFind.contains(object2));
        objectsGenerated.add(object);
        objectsGenerated.add(object2);
    }

    
    /**
     * Generate a unique object.
     * @param isRemove <code>true<code> if is remove <code>false</code> otherwise.
     * @return unique object.
     */
    public abstract K generateUniqueObject(boolean isRemove); 
    
    /**
     * Setter generic service.
     * @param genericService generic service.
     */
    public void setGenericService(GenericService genericService) {
        this.genericService = genericService;
    }

    /**
     * Getter objects generated.
     * @return objects generated. 
     */
    public List<K> getObjectsGenerated() {
        return objectsGenerated;
    }
    
    /**
     * clean all generated data. 
     * @throws AuctionInverseSystemException any system error.
     * @throws AuctionInverseValidationException any validation error.
     */
    @After
    public void cleanData() throws AuctionInverseSystemException,
                                                AuctionInverseValidationException {
        for (K item : objectsGenerated) {
            genericService.remove(item);
        }
    }
}
