import com.company.auctioninverse.model.businessobject.Aspirant;
import com.company.auctioninverse.model.businessobject.Inscribed;
import com.company.auctioninverse.model.businessobject.Offer;
import com.company.auctioninverse.model.businessobject.Round;
import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import com.company.auctioninverse.model.exceptions.AuctionInverseValidationException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test for class @link{RoundServiceImpl}.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */

public class RoundServiceImplTest extends AbstractGenericTest<Round>{
    @Before
    public void doBefore() {
        setGenericService(roundService);
    }
    
    @Test
    @Transactional
    @Override
    public void testSave() throws AuctionInverseValidationException, 
                                                        AuctionInverseSystemException {
        super.testSave(); 
    }

    @Test
    @Transactional
    @Override
    public void testRemove() throws AuctionInverseValidationException, 
                                                         AuctionInverseSystemException {
        super.testRemove();
    }

    @Test
    @Transactional
    @Override
    public void testFind() throws AuctionInverseValidationException, 
                                                          AuctionInverseSystemException {
        super.testFind();
    }

    @Test
    @Transactional
    @Override
    public void testFindAll() throws AuctionInverseValidationException,
                                                            AuctionInverseSystemException {
        super.testFindAll();
    }
    
    @Test
    @Transactional
    public void testUpdate() throws AuctionInverseSystemException, 
                                                        AuctionInverseValidationException {
        Round round = generateUniqueObject(Boolean.FALSE);
        roundService.save(round);
        round.setCreatedDate(new Date());
        roundService.update(round);
        Round roundFind = 
                roundService.find(round.getId(), Round.class);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy hh:mm aaa");
        String date1 = dateFormat.format(round.getCreatedDate());
        String date2 = dateFormat.format(roundFind.getCreatedDate());
        Assert.assertEquals(date1, date2);
        getObjectsGenerated().add(round);
    }
    
    @Test
    @Transactional
    public void testFindRoundsByAuction() throws AuctionInverseSystemException, 
                                                        AuctionInverseValidationException {
        Round round = generateUniqueObject(Boolean.FALSE);
        roundService.save(round);
        Round round2 = generateUniqueObject(Boolean.FALSE);
        round2.setAuction(round.getAuction());
        roundService.save(round2);
        List<Round> rounds = roundService.findRoundsByAuction(round.getAuction());
        Assert.assertTrue(rounds.contains(round));
        Assert.assertTrue(rounds.contains(round2));
        getObjectsGenerated().add(round);
        getObjectsGenerated().add(round2);
    }
    
    @Test
    @Transactional
    public void testFindWinnersByRound() throws AuctionInverseSystemException, 
                                                        AuctionInverseValidationException {
        Round round = generateUniqueObject(Boolean.FALSE);
        Inscribed inscribed = generateInscribed(Boolean.FALSE);
        Aspirant aspirant = inscribed.getAspirant();
        round.setAuction(aspirant.getAuction());
        Inscribed inscribed2 = generateInscribed(Boolean.FALSE);
        Aspirant aspirant2 = inscribed2.getAspirant();
        aspirant2.setAuction(aspirant.getAuction());
        inscribed2.setAspirant(aspirant2);
        List<Inscribed> winners = new ArrayList<Inscribed>();
        winners.add(inscribed);
        winners.add(inscribed2);
        round.setWinners(winners);
        roundService.save(round);
        List<Inscribed> inscribeds = roundService.findWinnerByRound(round);
        Assert.assertTrue(inscribeds.contains(inscribed));
        Assert.assertTrue(inscribeds.contains(inscribed2));
        getObjectsGenerated().add(round);
        round.setWinners(new ArrayList<Inscribed>());
        roundService.update(round);
    }
    
    @Test
    @Transactional
    public void testFindOffersByRound() throws AuctionInverseSystemException, 
                                                        AuctionInverseValidationException {
        Offer offer = generateOffer(Boolean.FALSE);
        Offer offer2 = generateOffer(Boolean.FALSE);
        offer2.setRound(offer.getRound());
        offerService.save(offer);
        offerService.save(offer2);
        List<Offer> offers = roundService.findOffersByRound(offer.getRound());
        Assert.assertTrue(offers.contains(offer));
        Assert.assertTrue(offers.contains(offer2));
        
        
    }
    
    @Test
    @Transactional
    public void testFindOfferByRoundInscribed() throws AuctionInverseSystemException, 
                                                        AuctionInverseValidationException {
        Offer offer = generateOffer(Boolean.FALSE);
        Offer offer2 = generateOffer(Boolean.FALSE);
        offer2.setRound(offer.getRound());
        offerService.save(offer);
        offerService.save(offer2);
        List<Offer> offers = roundService.
                findOfferByRoundInscribed(offer.getRound(), offer.getInscribed());
        Assert.assertTrue(offers.contains(offer));
        offers = roundService.
                findOfferByRoundInscribed(offer.getRound(), offer2.getInscribed());
        Assert.assertTrue(offers.contains(offer2));
        
    }
    
    @Transactional
    @Test
    public void testLoadCurrentRoundByAuction () 
            throws AuctionInverseSystemException, AuctionInverseValidationException {
        Round round = generateUniqueObject(Boolean.FALSE);
        round.setCurrent(Boolean.TRUE);
        roundService.save(round);
        Round roundFind = roundService.loadCurrentRoundByAuction(round.getAuction());
        Assert.assertEquals(roundFind.getId(), round.getId());
    }
    
    @Test
    @Transactional
    public void testQuantityRoundByAuction() throws AuctionInverseSystemException, 
                                                        AuctionInverseValidationException {
        Round round = generateUniqueObject(Boolean.FALSE);
        roundService.save(round);
        Round round2 = generateUniqueObject(Boolean.FALSE);
        round2.setAuction(round.getAuction());
        roundService.save(round2);
        Integer quantity = roundService.quantityRoundByAuction(round.getAuction());
        Assert.assertEquals(quantity, new Integer(2));
       
    }

    @Override
    public Round generateUniqueObject(boolean isRemove) {
        Round round = generateRound(isRemove);
        return round;
    }
    
    @Override
    public void cleanData() throws AuctionInverseSystemException, AuctionInverseValidationException {
        
    }
    
    
}
