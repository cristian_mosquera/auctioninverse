import com.company.auctioninverse.model.businessobject.AuctionProduct;
import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import com.company.auctioninverse.model.exceptions.AuctionInverseValidationException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test for class @link{AuctionProductServiceImpl}.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */

public class AuctionProductServiceImplTest extends AbstractGenericTest<AuctionProduct>{
    
    @Before
    public void doBefore() {
        setGenericService(auctionProductService);
    }
    
    @Test
    @Transactional
    @Override
    public void testSave() throws AuctionInverseValidationException, 
                                                        AuctionInverseSystemException {
        super.testSave(); 
    }

    @Test
    @Transactional
    @Override
    public void testRemove() throws AuctionInverseValidationException, 
                                                         AuctionInverseSystemException {
        super.testRemove();
    }

    @Test
    @Transactional
    @Override
    public void testFind() throws AuctionInverseValidationException, 
                                                          AuctionInverseSystemException {
        super.testFind();
    }

    @Test
    @Transactional
    @Override
    public void testFindAll() throws AuctionInverseValidationException,
                                                            AuctionInverseSystemException {
        super.testFindAll();
    }
    
    @Test
    @Transactional
    public void testUpdate() throws AuctionInverseSystemException, 
                                                        AuctionInverseValidationException {
        AuctionProduct auctionProduct = generateUniqueObject(Boolean.FALSE);
        auctionProductService.save(auctionProduct);
        auctionProduct.setQuantity(3);
        auctionProductService.update(auctionProduct);
        AuctionProduct auctionProductFind = auctionProductService.find(auctionProduct.getId(), AuctionProduct.class);
        Assert.assertEquals(auctionProductFind.getQuantity(), auctionProduct.getQuantity());
        getObjectsGenerated().add(auctionProduct);
    }
    
     
    
    @Override
    public AuctionProduct generateUniqueObject(boolean isRemove) {
        AuctionProduct auctionProduct = generateAuctionProduct(isRemove);
        return auctionProduct;
    }

    @Override
    public void cleanData() throws AuctionInverseSystemException, AuctionInverseValidationException {
    
    }
    
    
    
}
