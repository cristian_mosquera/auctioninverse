
import com.company.auctioninverse.model.businessobject.Auction;
import com.company.auctioninverse.model.businessobject.Round;
import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import com.company.auctioninverse.model.exceptions.AuctionInverseValidationException;
import com.company.auctioninverse.model.service.impl.RoundServiceImpl;
import com.company.auctioninverse.model.thread.RoundTimerTask;
import java.util.Timer;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Mosquera</a>
 */
public class RoundTimerTaskTest {
    
    public static void main(String args[]) 
            throws AuctionInverseSystemException, AuctionInverseValidationException, InterruptedException {
        
        Round round = new Round();
        round.setId(Long.MIN_VALUE);
        Auction auction = new Auction();
        auction.setTimeForRound(20000l);
        round.setAuction(auction);
        final Timer timer = new Timer();
        timer.schedule(new RoundTimerTask(round, timer, null), 0, 1234);
        RoundServiceImpl roundServiceImpl = new RoundServiceImpl();
        roundServiceImpl.loadTimeTranscurredByRound(round);
        System.out.print(round.getTimeTranscurred());
        roundServiceImpl.loadTimeTranscurredByRound(round);
        System.out.print(round.getTimeTranscurred());
        roundServiceImpl.loadTimeTranscurredByRound(round);
        System.out.print(round.getTimeTranscurred());
        Thread.sleep(1000l);
        roundServiceImpl.loadTimeTranscurredByRound(round);
        System.out.print(round.getTimeTranscurred());
    }
}
