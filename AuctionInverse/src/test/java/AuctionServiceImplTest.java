import com.company.auctioninverse.model.businessobject.Admin;
import com.company.auctioninverse.model.businessobject.Aspirant;
import com.company.auctioninverse.model.businessobject.Auction;
import com.company.auctioninverse.model.businessobject.AuctionProduct;
import com.company.auctioninverse.model.businessobject.Inscribed;
import com.company.auctioninverse.model.businessobject.Offer;
import com.company.auctioninverse.model.businessobject.Round;
import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import com.company.auctioninverse.model.exceptions.AuctionInverseValidationException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test for class @link{AuctionServiceImpl}.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */

public class AuctionServiceImplTest extends AbstractGenericTest<Auction>{
    
    @Before
    public void doBefore() {
        setGenericService(auctionService);
    }
    
    @Test
    @Transactional
    @Override
    public void testSave() throws AuctionInverseValidationException, 
                                                        AuctionInverseSystemException {
        super.testSave(); 
    }

    @Test
    @Transactional
    @Override
    public void testRemove() throws AuctionInverseValidationException, 
                                                         AuctionInverseSystemException {
        super.testRemove();
    }

    @Test
    @Transactional
    @Override
    public void testFind() throws AuctionInverseValidationException, 
                                                          AuctionInverseSystemException {
        super.testFind();
    }

    @Test
    @Transactional
    @Override
    public void testFindAll() throws AuctionInverseValidationException,
                                                            AuctionInverseSystemException {
        super.testFindAll();
    }
    
    @Test
    @Transactional
    public void testUpdate() throws AuctionInverseSystemException, 
                                                        AuctionInverseValidationException {
        Auction auction = generateUniqueObject(Boolean.FALSE);
        auctionService.save(auction);
        auction.setDescription("nombre editado");
        auctionService.update(auction);
        Auction auctionFind = 
                auctionService.find(auction.getId(), Auction.class);
        Assert.assertEquals(auctionFind.getDescription(), auction.getDescription());
        getObjectsGenerated().add(auction);
    }
    
    @Transactional
    @Test
    public void testFindAuctionsByAdmin() 
            throws AuctionInverseSystemException, AuctionInverseValidationException {
        Auction auction = generateUniqueObject(Boolean.FALSE);
        auction.setName("name to find");
        Admin admin = auction.getConvenning().getAdmin();
        auctionService.save(auction);
        Aspirant aspirant = generateAspirant(Boolean.FALSE);
        aspirant.setAuction(auction);
        aspirantService.save(aspirant);
        Auction auction2 = generateUniqueObject(Boolean.FALSE);
        auction2.getConvenning().setAdmin(admin);
        auction2.setName("name to find");
        auctionService.save(auction2);
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -1);
        Date startDate = calendar.getTime();
        calendar.add(Calendar.DAY_OF_YEAR, 2);
        Date endDate = calendar.getTime();
        List<Auction> auctions =  
                auctionService.findAuctionsByAdmin(auction.getConvenning().getAdmin(), 
                        auction.getStatus(), startDate, endDate, "name to find", null);
        
        Assert.assertTrue(auctions.contains(auction));
        Assert.assertTrue(auctions.contains(auction2));
        auctions =  auctionService.findAuctionsByAdmin(auction.getConvenning().getAdmin(), 
                        auction.getStatus(), startDate, endDate, "name to find", aspirant);
        Iterator<Auction> iterator = auctions.iterator();
        while (iterator.hasNext()) {
            Auction auction1 = iterator.next();
            if (auction1.getAspirants() != null) {
                Iterator<Aspirant> itAspirant = auction1.getAspirants().iterator();
                if (itAspirant.hasNext()) {
                    Assert.assertTrue(auction1.getAspirants().contains(aspirant));
                    
                }
            }
        }
        
    }
    
    @Transactional
    @Test
    public void testFindAuctionsByInscribed() 
            throws AuctionInverseSystemException, AuctionInverseValidationException {
        Auction auction = generateUniqueObject(Boolean.FALSE);
        auction.setName("name to find");
        Inscribed inscribed = generateInscribed(Boolean.FALSE);
        Aspirant aspirant = generateAspirant(Boolean.FALSE);
        aspirant.setAuction(auction);
        inscribed.setAspirant(aspirant);
        inscribedService.save(inscribed);
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -1);
        Date startDate = calendar.getTime();
        calendar.add(Calendar.DAY_OF_YEAR, 2);
        Date endDate = calendar.getTime();
        List<Auction> auctions =  
                auctionService.findAuctionsByInscribed(inscribed ,
                        auction.getStatus(), startDate, endDate, "name to find");
        Assert.assertTrue(auctions.contains(auction));
    }
    
    @Transactional
    @Test
    public void testFindAuctionProductByAuction() 
            throws AuctionInverseSystemException, AuctionInverseValidationException {
        
        AuctionProduct auctionProduct = generateAuctionProduct(Boolean.FALSE);
        AuctionProduct auctionProduct2 = generateAuctionProduct(Boolean.FALSE);
        auctionProduct2.setAuction(auctionProduct.getAuction());
        auctionProductService.save(auctionProduct);
        auctionProductService.save(auctionProduct2);
        List<AuctionProduct> auctionProductsFind =  
                auctionService.findAuctionProductByAuction(auctionProduct.getAuction());
        Assert.assertTrue(auctionProductsFind.contains(auctionProduct));
        Assert.assertTrue(auctionProductsFind.contains(auctionProduct2));
    }
    
    @Transactional
    @Test
    public void testFindWinnersByAuction() 
            throws AuctionInverseSystemException, AuctionInverseValidationException {
        
        Auction auction = generateAuction(Boolean.FALSE);
        List<Inscribed> inscribeds = new ArrayList<Inscribed>();
        Inscribed inscribed = generateInscribed(Boolean.FALSE);
        inscribeds.add(inscribed);
        Inscribed inscribed2 = generateInscribed(Boolean.FALSE);
        inscribeds.add(inscribed2);
        auction.setWinners(inscribeds);
        auctionService.save(auction);
        List<Inscribed> inscribedsFind =  
                auctionService.findWinnersByAuction(auction);
        Assert.assertTrue(inscribedsFind.contains(inscribed));
        Assert.assertTrue(inscribedsFind.contains(inscribed2));
        auction.setWinners(new ArrayList<Inscribed>());
        auctionService.update(auction);
    }
    
    @Transactional
    @Test
    public void testStartAuction() throws AuctionInverseSystemException, 
                                                    AuctionInverseValidationException {
        Inscribed inscribed = generateInscribed(Boolean.FALSE);
        Auction auction = generateAuction(Boolean.FALSE);
        auction.setStatus("STARTED");
        auctionService.save(auction);
        Aspirant aspirant = generateAspirant(Boolean.FALSE);
        aspirant.setAuction(auction);
        inscribed.setAspirant(aspirant);
        inscribedService.save(inscribed);
        auctionService.startAuction(auction);
        Assert.assertEquals("STARTED", auction.getStatus());
        
        
        List<Round> rounds = roundService.findRoundsByAuction(auction);
        Assert.assertEquals("INPROGRESS", rounds.iterator().next().getStatus());
        
    }
    
    @Transactional
    @Test
    public void testSaveOffer() throws AuctionInverseSystemException, 
                                                    AuctionInverseValidationException {
        Inscribed inscribed = generateInscribed(Boolean.FALSE);
        Auction auction = generateAuction(Boolean.FALSE);
        auction.setStatus("STARTED");
        auctionService.save(auction);
        Aspirant aspirant = generateAspirant(Boolean.FALSE);
        aspirant.setAuction(auction);
        inscribed.setAspirant(aspirant);
        inscribedService.save(inscribed);
        auctionService.startAuction(auction);
        Assert.assertEquals("STARTED", auction.getStatus());
        List<Round> rounds = roundService.findRoundsByAuction(auction);
        Round round = rounds.iterator().next();
        Assert.assertEquals("INPROGRESS", round.getStatus());
        Offer offer = new Offer();
        offer.setInscribed(inscribed);
        offer.setRound(round);
        offer.setValue(Double.NaN);
        auctionService.saveOffer(offer);
        Offer offerFind = offerService.find(offer.getId(), Offer.class);
        Assert.assertEquals(offer, offerFind);
        
    }
    
    @Transactional
    @Test
    public void testNewRound() throws AuctionInverseSystemException, 
                                                    AuctionInverseValidationException {
        Inscribed inscribed = generateInscribed(Boolean.FALSE);
        Auction auction = generateAuction(Boolean.FALSE);
        auction.setStatus("STARTED");
        auctionService.save(auction);
        Aspirant aspirant = generateAspirant(Boolean.FALSE);
        aspirant.setAuction(auction);
        inscribed.setAspirant(aspirant);
        inscribedService.save(inscribed);
        Round round = new Round();
        round.setAuction(auction);
        auctionService.newRound(round);
        
        List<Round> rounds = roundService.findRoundsByAuction(auction);
        
        
    }    
     
    @Override
    public Auction generateUniqueObject(boolean isRemove) {
        Auction auction = generateAuction(isRemove);
        return auction;
    }

    @Override
    public void cleanData() throws AuctionInverseSystemException, AuctionInverseValidationException {
        
    }
    
    
    
}
