import com.company.auctioninverse.model.businessobject.Offer;
import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import com.company.auctioninverse.model.exceptions.AuctionInverseValidationException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test for class @link{OfferServiceImpl}.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */

public class OfferServiceImplTest extends AbstractGenericTest<Offer>{
    @Before
    public void doBefore() {
        setGenericService(inscribedService);
    }
    
    @Test
    @Transactional
    @Override
    public void testSave() throws AuctionInverseValidationException, 
                                                        AuctionInverseSystemException {
        super.testSave(); 
    }

//    @Test
    @Transactional
    @Override
    public void testRemove() throws AuctionInverseValidationException, 
                                                         AuctionInverseSystemException {
        super.testRemove();
    }

//    @Test
    @Transactional
    @Override
    public void testFind() throws AuctionInverseValidationException, 
                                                          AuctionInverseSystemException {
        super.testFind();
    }

//    @Test
    @Transactional
    @Override
    public void testFindAll() throws AuctionInverseValidationException,
                                                            AuctionInverseSystemException {
        super.testFindAll();
    }
    
//    @Test
    @Transactional
    public void testUpdate() throws AuctionInverseSystemException, 
                                                        AuctionInverseValidationException {
        Offer offer = generateUniqueObject(Boolean.FALSE);
        offerService.save(offer);
        offer.setValue(Double.MAX_VALUE);
        offerService.update(offer);
        Offer offerFind = 
                offerService.find(offer.getId(), Offer.class);
        Assert.assertEquals(offerFind.getValue(), offer.getValue());
        getObjectsGenerated().add(offer);
    }

    @Override
    public Offer generateUniqueObject(boolean isRemove) {
        Offer offer = generateOffer(isRemove);
        return offer;
    }
    
    @Override
    public void cleanData() throws AuctionInverseSystemException, AuctionInverseValidationException {
        
    }
    
}
