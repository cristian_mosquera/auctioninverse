
import com.company.auctioninverse.model.businessobject.Auction;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class TestClass {
    public long myLong = 1234;
    public static long timeExcecute = 5000;
    public static Date initialExcecute = new Date();
    private Auction auction;

        public static void main(String[] args) {
        final TestClass test = new TestClass();

       final Timer timer = new Timer();
        timer.schedule(new TimerTask() {

            @Override
            public void run() {
                long milliseconsTranscurred = new Date().getTime() - initialExcecute.getTime();
                test.doStuff(milliseconsTranscurred);
                if (milliseconsTranscurred >= timeExcecute) {
                    timer.cancel();
                }
            }

            
        }, 0, test.myLong);
        System.out.printf("Pasado");
    }

    public void doStuff(long milliseconds){
        Long diffSeconds = milliseconds / 1000 % 60;  
        Long diffMinutes = milliseconds / (60 * 1000) % 60;
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MINUTE, diffMinutes.intValue());
        calendar.set(Calendar.SECOND, diffSeconds.intValue());
        String formatHour = new SimpleDateFormat("mm:ss").format(calendar.getTime());
        System.out.printf("hola " + formatHour);
    }
}