import com.company.auctioninverse.model.businessobject.Admin;
import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import com.company.auctioninverse.model.exceptions.AuctionInverseValidationException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test for class @link{AdminServiceImpl}.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */

public class AdminServiceImplTest extends AbstractGenericTest<Admin>{
    
    @Before
    public void doBefore() {
        setGenericService(adminService);
    }
    
    @Test
    @Transactional
    @Override
    public void testSave() throws AuctionInverseValidationException, 
                                                        AuctionInverseSystemException {
        super.testSave(); 
    }

    @Test
    @Transactional
    @Override
    public void testRemove() throws AuctionInverseValidationException, 
                                                         AuctionInverseSystemException {
        super.testRemove();
    }

    @Test
    @Transactional
    @Override
    public void testFind() throws AuctionInverseValidationException, 
                                                          AuctionInverseSystemException {
        super.testFind();
    }

    @Test
    @Transactional
    @Override
    public void testFindAll() throws AuctionInverseValidationException,
                                                            AuctionInverseSystemException {
        super.testFindAll();
    }
    
    @Test
    @Transactional
    public void testUpdate() throws AuctionInverseSystemException, 
                                                        AuctionInverseValidationException {
        Admin admin = generateUniqueObject(Boolean.FALSE);
        adminService.save(admin);
        admin.setName("nombre editado");
        adminService.update(admin);
        Admin adminFind = adminService.find(admin.getId(), Admin.class);
        Assert.assertEquals(adminFind.getName(), admin.getName());
        getObjectsGenerated().add(admin);
    }
    
     
    
    @Override
    public Admin generateUniqueObject(boolean isRemove) {
        Admin admin = generateAdmin(isRemove);
        return admin;
    }
    
}
