
import com.company.auctioninverse.model.businessobject.Admin;
import com.company.auctioninverse.model.businessobject.Aspirant;
import com.company.auctioninverse.model.businessobject.Auction;
import com.company.auctioninverse.model.businessobject.AuctionProduct;
import com.company.auctioninverse.model.businessobject.Convenning;
import com.company.auctioninverse.model.businessobject.Inscribed;
import com.company.auctioninverse.model.businessobject.Offer;
import com.company.auctioninverse.model.businessobject.Product;
import com.company.auctioninverse.model.businessobject.ProductCategory;
import com.company.auctioninverse.model.businessobject.Rol;
import com.company.auctioninverse.model.businessobject.Round;
import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import com.company.auctioninverse.model.exceptions.AuctionInverseValidationException;
import com.company.auctioninverse.model.service.AdminService;
import com.company.auctioninverse.model.service.AspirantService;
import com.company.auctioninverse.model.service.AuctionProductService;
import com.company.auctioninverse.model.service.AuctionService;
import com.company.auctioninverse.model.service.ConvenningService;
import com.company.auctioninverse.model.service.InscribedService;
import com.company.auctioninverse.model.service.OfferService;
import com.company.auctioninverse.model.service.ProductCategoryService;
import com.company.auctioninverse.model.service.ProductService;
import com.company.auctioninverse.model.service.RolService;
import com.company.auctioninverse.model.service.RoundService;
import com.company.common.model.businessobject.ObjectValue;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.Ignore;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Class responsible of generic test auction inverse.
 *
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Mosquera</a>
 */
@Ignore
public abstract class AbstractGenericTest<k extends ObjectValue> extends GenericTest<k> {

    protected List<Convenning> convenningGenerateds = new ArrayList<Convenning>();

    protected List<Admin> adminGenerateds = new ArrayList<Admin>();

    protected List<Auction> auctionGenerateds = new ArrayList<Auction>();

    protected List<Offer> offersGenerateds = new ArrayList<Offer>();

    protected List<Aspirant> aspirantGenerateds = new ArrayList<Aspirant>();

    protected List<Inscribed> inscribedGenerateds = new ArrayList<Inscribed>();

    protected List<Round> roundGenerateds = new ArrayList<Round>();
    
    
    protected List<ProductCategory> productCategoryGenerateds = new ArrayList<ProductCategory>();

    @Autowired
    protected InscribedService inscribedService;
    
    @Autowired
    protected ProductService productService;
    
    @Autowired
    protected ProductCategoryService productCategoryService;

    @Autowired
    protected ConvenningService convenningService;

    @Autowired
    protected AdminService adminService;

    @Autowired
    protected AuctionService auctionService;

    @Autowired
    protected AspirantService aspirantService;

    @Autowired
    protected OfferService offerService;

    @Autowired
    protected RoundService roundService;
    
    @Autowired
    protected RolService rolService;
    
    @Autowired
    protected AuctionProductService auctionProductService;

    /**
     * Method responsible of generate a offer.
     *
     * @return offer.
     */
    public Offer generateOffer(boolean isRemove) {
        Offer offer = new Offer();
        offer.setCreatedDate(new Date());
        offer.setUpdatedDate(new Date());
        offer.setValue(Double.NaN);
        offer.setInscribed(generateInscribed(isRemove));
        Round round = generateRound(isRemove);
        round.setStatus("INPROGRESS");
        offer.setRound(round);
        if (!isRemove) {
            offersGenerateds.add(offer);
        }
        return offer;
    }
    
    /**
     * Method responsible of generate a product.
     *
     * @return product generated.
     */
    public Product generateProduct(boolean isRemove) {
        Product product = new Product();
        product.setCreatedDate(new Date());
        product.setUpdatedDate(new Date());
        product.setDescription("null");
        product.setName("name");
        product.setProductCategory(generateProductCategory(isRemove));
        return product;
    }
    
    /**
     * Method responsible of generate a product category.
     *
     * @return product category generated.
     */
    public ProductCategory generateProductCategory(boolean isRemove) {
        ProductCategory productCategory = new ProductCategory();
        productCategory.setCreatedDate(new Date());
        productCategory.setDescription("description");
        productCategory.setName("name");
        productCategory.setUpdatedDate(new Date());
        if (!isRemove) {
            productCategoryGenerateds.add(productCategory);
        }
        return productCategory;
    }
    
     /**
     * Method responsible of generate a rol.
     *
     * @return rol generated.
     */
    public Rol generateRol(boolean isRemove) {
        Rol rol = new Rol();
        rol.setName("name");
        
        return rol;
    }
    
    /**
     * Method responsible of generate a round.
     *
     * @return round.
     */
    public Round generateRound(boolean isRemove) {
        Round round = new Round();
        Inscribed inscribed = generateInscribed(isRemove);
        Aspirant aspirant = inscribed.getAspirant();
        Auction auction = aspirant.getAuction();
        Inscribed inscribed2 = generateInscribed(isRemove);
        Aspirant aspirant2 = inscribed2.getAspirant();
        aspirant2.setAuction(auction);
        inscribed2.setAspirant(aspirant2);
        round.setAuction(aspirant.getAuction());
        round.setUpdatedDate(new Date());
        round.setCreatedDate(new Date());
        round.setNumber(Short.MIN_VALUE);
        round.setStatus("INPROGRESS");
        round.setCurrent(false);
        Offer offer = new Offer();
        offer.setInscribed(inscribed);
        offer.setRound(round);
        offer.setValue(Double.NaN);
        offer.setCreatedDate(new Date());
        offer.setUpdatedDate(new Date());
        Offer offer2 = new Offer();
        offer2.setInscribed(inscribed);
        offer2.setRound(round);
        offer2.setValue(Double.NaN);
        offer2.setCreatedDate(new Date());
        offer2.setUpdatedDate(new Date());
        List<Offer> offers = new ArrayList<Offer>();
        offers.add(offer);
        offers.add(offer2);
        round.setOffers(offers);
        
        if (!isRemove) {
            roundGenerateds.add(round);
        }
        return round;
    }

    /**
     * Method responsible of generate a auction.
     *
     * @return auction.
     */
    public Auction generateAuction(boolean isRemove) {
        Auction auction = new Auction();
        auction.setName("name");
        auction.setStatus("CREATED");
        auction.setDescription("description");
        auction.setStartValue(Double.NaN);
        auction.setCreatedDate(new Date());
        auction.setUpdatedDate(new Date());
        auction.setStartDate(new Date());
        auction.setConvenning(generateConvenning(isRemove));
        if (!isRemove) {
            auctionGenerateds.add(auction);
        }
        return auction;
    }

    /**
     * Method responsible of generate a convenning.
     *
     * @return convenning
     */
    public Convenning generateConvenning(boolean isRemove) {
        Convenning convenning = new Convenning();
        Long uiid = Long.valueOf(Double.valueOf(Math.random() * 100000).longValue());
        convenning.setDescription("Convocatooria" + uiid);
        convenning.setCreatedDate(new Date());
        convenning.setUpdatedDate(new Date());
        convenning.setStartDate(new Date());
        convenning.setEndDate(new Date());
        if (!isRemove) {
            convenningGenerateds.add(convenning);
        }
        Admin admin = generateAdmin(isRemove);
        convenning.setAdmin(admin);
        return convenning;
    }

    /**
     * Method responsible of generate a inscribed.
     *
     * @return inscribed.
     */
    public Inscribed generateInscribed(boolean isRemove) {
        Long uiid = Long.valueOf(Double.valueOf(Math.random() * 100000).longValue());
        Aspirant aspirant = generateAspirant(isRemove);
        Inscribed inscribed = new Inscribed();
        inscribed.setBirthDate(new Date());
        inscribed.setCreatedDate(new Date());
        inscribed.setEmail("cmp166@hotmail.es");
        inscribed.setGener("M");
        inscribed.setIdentificationNumber("1128057876");
        inscribed.setIdentificationType("CC");
        inscribed.setLastName("Mosquera");
        inscribed.setName("Cristian");
        inscribed.setPassword("123456");
        inscribed.setPhone("phone");
        inscribed.setAspirant(aspirant);
        inscribed.setStatus("ACTIVE");
        Rol rol = new Rol();
        rol.setName("Admin");
        inscribed.setRol(rol);
        inscribed.setUpdatedDate(new Date());
        inscribed.setUserName("cmosquera" + uiid);
        if (!isRemove) {
            inscribedGenerateds.add(inscribed);
        }
        return inscribed;
    }

    /**
     * Generate a aspirant.
     *
     * @return a aspirant.
     */
    public Aspirant generateAspirant(boolean isRemove) {
        Long uiid = Long.valueOf(Double.valueOf(Math.random() * 100000).longValue());
        Auction auction = generateAuction(isRemove);
        Aspirant aspirant = new Aspirant();
        aspirant.setBirthDate(new Date());
        aspirant.setCreatedDate(new Date());
        aspirant.setEmail("cmp166@hotmail.es");
        aspirant.setGener("M");
        aspirant.setIdentificationNumber("1128057876");
        aspirant.setIdentificationType("CC");
        aspirant.setLastName("Mosquera");
        aspirant.setName("Cristian");
        aspirant.setPassword("123456");
        aspirant.setStatus("ACTIVE");
        Rol rol = new Rol();
        rol.setName("Admin");
        aspirant.setRol(rol);
        aspirant.setUpdatedDate(new Date());
        aspirant.setUserName("cmosquera" + uiid);
        aspirant.setAuction(auction);
        aspirant.setPhone("phone");
        if (!isRemove) {
            aspirantGenerateds.add(aspirant);
        }
        return aspirant;
    }

    /**
     * Generate a unique admin.
     *
     * @return a unique admin.
     */
    public Admin generateAdmin(boolean isRemove) {
        Long uiid = Long.valueOf(Double.valueOf(Math.random() * 100000).longValue());
        Admin admin = new Admin();
        admin.setStatus("ACTIVE");
        admin.setBirthDate(new Date());
        admin.setCreatedDate(new Date());
        admin.setEmail("cmp166@hotmail.es");
        admin.setGener("M");
        admin.setIdentificationNumber("1128057876");
        admin.setIdentificationType("CC");
        admin.setLastName("Mosquera");
        admin.setName("Cristian");
        admin.setPassword("123456");
        Rol rol = new Rol();
        rol.setName("Admin");
        admin.setRol(rol);
        admin.setUpdatedDate(new Date());
        admin.setUserName("cmosquera" + uiid);
        admin.setPhone("phone");
        if (!isRemove) {
            adminGenerateds.add(admin);
        }
        return admin;
    }
    
    /**
     * Method responsible of generate a auction poduct
     * @param isRemove
     * @return a unique auction product.
     */
    public AuctionProduct generateAuctionProduct(boolean isRemove) {
        Auction auction = generateAuction(isRemove);
        AuctionProduct auctionProduct = new AuctionProduct();
        auctionProduct.setAuction(auction);
        auctionProduct.setPrice(Double.NaN);
        auctionProduct.setProduct(generateProduct(Boolean.FALSE));
        auctionProduct.setQuantity(2);
        return auctionProduct;
    }

    @Override
    public void cleanData() throws AuctionInverseSystemException, AuctionInverseValidationException {
        for (Offer offer : offersGenerateds) {
            offerService.remove(offer);
        }

        for (Inscribed inscribed : inscribedGenerateds) {
            inscribedService.remove(inscribed);
        }

        for (Aspirant aspirant : aspirantGenerateds) {
            aspirantService.remove(aspirant);
        }

        for (Round round : roundGenerateds) {
            roundService.remove(round);
        }

        for (Auction auction : auctionGenerateds) {
            auctionService.remove(auction);
        }

        for (Convenning convenning : convenningGenerateds) {
            convenningService.remove(convenning);
        }

        for (Admin admin : adminGenerateds) {
            adminService.remove(admin);
        }
        try {
            super.cleanData();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }

}
