import com.company.auctioninverse.model.businessobject.Product;
import com.company.auctioninverse.model.businessobject.ProductCategory;
import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import com.company.auctioninverse.model.exceptions.AuctionInverseValidationException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test for class @link{ProductServiceImpl}.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */

public class ProductServiceImplTest extends AbstractGenericTest<Product>{
    @Before
    public void doBefore() {
        setGenericService(productService);
    }
    
    @Test
    @Transactional
    @Override
    public void testSave() throws AuctionInverseValidationException, 
                                                        AuctionInverseSystemException {
        super.testSave(); 
    }

    @Test
    @Transactional
    @Override
    public void testRemove() throws AuctionInverseValidationException, 
                                                         AuctionInverseSystemException {
        super.testRemove();
    }

    @Test
    @Transactional
    @Override
    public void testFind() throws AuctionInverseValidationException, 
                                                          AuctionInverseSystemException {
        super.testFind();
    }

    @Test
    @Transactional
    @Override
    public void testFindAll() throws AuctionInverseValidationException,
                                                            AuctionInverseSystemException {
        super.testFindAll();
    }
    
    @Test
    @Transactional
    public void testUpdate() throws AuctionInverseSystemException, 
                                                        AuctionInverseValidationException {
        Product product = generateUniqueObject(Boolean.FALSE);
        productService.save(product);
        product.setName("Nombre editado");
        productService.update(product);
        Product prductFind = 
                productService.find(product.getId(), Product.class);
        Assert.assertEquals(prductFind.getName(), product.getName());
        getObjectsGenerated().add(product);
    }

    @Override
    public Product generateUniqueObject(boolean isRemove) {
        Product product = generateProduct(isRemove);
        return product;
    }

    @Override
    public void cleanData() throws AuctionInverseSystemException, AuctionInverseValidationException {
        super.cleanData(); 
        for (ProductCategory productCategory : productCategoryGenerateds) {
            productCategoryService.remove(productCategory);
        } 
        
    }
     
    
    
    
}
