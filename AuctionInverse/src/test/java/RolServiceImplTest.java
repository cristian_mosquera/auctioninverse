import com.company.auctioninverse.model.businessobject.Rol;
import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import com.company.auctioninverse.model.exceptions.AuctionInverseValidationException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test for class @link{RolServiceImpl}.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */

public class RolServiceImplTest extends AbstractGenericTest<Rol>{
    
    @Before
    public void doBefore() {
        setGenericService(rolService);
    }
    
    @Test
    @Transactional
    @Override
    public void testSave() throws AuctionInverseValidationException, 
                                                        AuctionInverseSystemException {
        super.testSave(); 
    }

    @Test
    @Transactional
    @Override
    public void testRemove() throws AuctionInverseValidationException, 
                                                         AuctionInverseSystemException {
        super.testRemove();
    }

    @Test
    @Transactional
    @Override
    public void testFind() throws AuctionInverseValidationException, 
                                                          AuctionInverseSystemException {
        super.testFind();
    }

    @Test
    @Transactional
    @Override
    public void testFindAll() throws AuctionInverseValidationException,
                                                            AuctionInverseSystemException {
        super.testFindAll();
    }
    
    @Test
    @Transactional
    public void testUpdate() throws AuctionInverseSystemException, 
                                                        AuctionInverseValidationException {
        Rol  rol = generateUniqueObject(Boolean.FALSE);
        rolService.save(rol);
        rol.setName("nombre editado");
        rolService.update(rol);
        Rol rolFind = rolService.
                find(rol.getId(),  Rol.class);
        Assert.assertEquals(rolFind.getName(), rol.getName());
        getObjectsGenerated().add(rol);
    }
    
     
    @Override
    public Rol generateUniqueObject(boolean isRemove) {
        Rol rol = generateRol(isRemove);
        return rol;
    }
    
}
