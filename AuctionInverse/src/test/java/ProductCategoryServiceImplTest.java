import com.company.auctioninverse.model.businessobject.Admin;
import com.company.auctioninverse.model.businessobject.ProductCategory;
import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import com.company.auctioninverse.model.exceptions.AuctionInverseValidationException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test for class @link{ProductCategoryServiceImpl}.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */

public class ProductCategoryServiceImplTest extends AbstractGenericTest<ProductCategory>{
    
    @Before
    public void doBefore() {
        setGenericService(productCategoryService);
    }
    
    @Test
    @Transactional
    @Override
    public void testSave() throws AuctionInverseValidationException, 
                                                        AuctionInverseSystemException {
        super.testSave(); 
    }

    @Test
    @Transactional
    @Override
    public void testRemove() throws AuctionInverseValidationException, 
                                                         AuctionInverseSystemException {
        super.testRemove();
    }

    @Test
    @Transactional
    @Override
    public void testFind() throws AuctionInverseValidationException, 
                                                          AuctionInverseSystemException {
        super.testFind();
    }

    @Test
    @Transactional
    @Override
    public void testFindAll() throws AuctionInverseValidationException,
                                                            AuctionInverseSystemException {
        super.testFindAll();
    }
    
    @Test
    @Transactional
    public void testUpdate() throws AuctionInverseSystemException, 
                                                        AuctionInverseValidationException {
        ProductCategory productCategory = generateUniqueObject(Boolean.FALSE);
        productCategoryService.save(productCategory);
        productCategory.setName("nombre editado");
        productCategoryService.update(productCategory);
        ProductCategory productCategoryFind = productCategoryService.
                find(productCategory.getId(),  ProductCategory.class);
        Assert.assertEquals(productCategoryFind.getName(), productCategory.getName());
        getObjectsGenerated().add(productCategory);
    }
    
     
    
    @Override
    public ProductCategory generateUniqueObject(boolean isRemove) {
        ProductCategory productCategory = generateProductCategory(isRemove);
        return productCategory;
    }
    
}
