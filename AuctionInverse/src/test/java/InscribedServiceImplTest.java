import com.company.auctioninverse.model.businessobject.Inscribed;
import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import com.company.auctioninverse.model.exceptions.AuctionInverseValidationException;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test for class @link{InscribedServiceImpl}.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */

public class InscribedServiceImplTest extends AbstractGenericTest<Inscribed>{
    
    @Before
    public void doBefore() {
        setGenericService(inscribedService);
    }
    
    @Test
    @Transactional
    @Override
    public void testSave() throws AuctionInverseValidationException, 
                                                        AuctionInverseSystemException {
        super.testSave(); 
    }

    @Test
    @Transactional
    @Override
    public void testRemove() throws AuctionInverseValidationException, 
                                                         AuctionInverseSystemException {
        super.testRemove();
    }

    @Test
    @Transactional
    @Override
    public void testFind() throws AuctionInverseValidationException, 
                                                          AuctionInverseSystemException {
        super.testFind();
    }

    @Test
    @Transactional
    @Override
    public void testFindAll() throws AuctionInverseValidationException,
                                                            AuctionInverseSystemException {
        super.testFindAll();
    }
    
    @Test
    @Transactional
    public void testUpdate() throws AuctionInverseSystemException, 
                                                        AuctionInverseValidationException {
        Inscribed inscribed = generateUniqueObject(Boolean.FALSE);
        inscribedService.save(inscribed);
        inscribed.setName("nombre editado");
        inscribedService.update(inscribed);
        Inscribed inscribedFind = 
                inscribedService.find(inscribed.getId(), Inscribed.class);
        Assert.assertEquals(inscribedFind.getName(), inscribed.getName());
        getObjectsGenerated().add(inscribed);
    }
    
   @Test
    public void testFindInscribedByAuction() throws AuctionInverseSystemException, 
                                                        AuctionInverseValidationException {
        Inscribed inscribed = generateUniqueObject(Boolean.FALSE);
        inscribedService.save(inscribed);
        List<Inscribed> inscribeds = inscribedService.
                    findInscribedByAuction(inscribed.getAspirant().getAuction());
        Assert.assertTrue(inscribeds.contains(inscribed));
    }
    
     
    
    @Override
    public Inscribed generateUniqueObject(boolean isRemove) {
        Inscribed inscribed = generateInscribed(isRemove);
        return inscribed;
    }

    @Override
    public void cleanData() throws AuctionInverseSystemException, AuctionInverseValidationException {
        
    }
    
    
    
}
