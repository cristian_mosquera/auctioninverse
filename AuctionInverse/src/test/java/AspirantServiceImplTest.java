import com.company.auctioninverse.model.businessobject.Aspirant;
import com.company.auctioninverse.model.businessobject.Auction;
import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import com.company.auctioninverse.model.exceptions.AuctionInverseValidationException;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test for class @link{AspirantServiceImpl}.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */

public class AspirantServiceImplTest extends AbstractGenericTest<Aspirant>{
    
    @Before
    public void doBefore() {
        setGenericService(aspirantService);
    }
    
    @Test
    @Transactional
    @Override
    public void testSave() throws AuctionInverseValidationException, 
                                                        AuctionInverseSystemException {
        super.testSave(); 
    }

    @Test
    @Transactional
    @Override
    public void testRemove() throws AuctionInverseValidationException, 
                                                         AuctionInverseSystemException {
        super.testRemove();
    }

    @Test
    @Transactional
    @Override
    public void testFind() throws AuctionInverseValidationException, 
                                                          AuctionInverseSystemException {
        super.testFind();
    }

    @Test
    @Transactional
    @Override
    public void testFindAll() throws AuctionInverseValidationException,
                                                            AuctionInverseSystemException {
        super.testFindAll();
    }
    
    @Test
    @Transactional
    public void testUpdate() throws AuctionInverseSystemException, 
                                                        AuctionInverseValidationException {
        Aspirant aspirant = generateUniqueObject(Boolean.FALSE);
        aspirantService.save(aspirant);
        aspirant.setName("nombre editado");
        aspirantService.update(aspirant);
        Aspirant aspirantFind = 
                aspirantService.find(aspirant.getId(), Aspirant.class);
        Assert.assertEquals(aspirantFind.getName(), aspirant.getName());
        getObjectsGenerated().add(aspirant);
    }
    
    @Test
    @Transactional
    public void testaspirantByAuction() throws AuctionInverseSystemException, 
                                                        AuctionInverseValidationException {
        Aspirant aspirant = generateUniqueObject(Boolean.FALSE);
        Auction auction = generateAuction(Boolean.FALSE);
        auctionService.save(auction);
        aspirant.setAuction(auction);
        aspirantService.save(aspirant);
        List<Aspirant> aspirants =  aspirantService.findAspirantsByAuction(auction);
        Assert.assertTrue(aspirants.contains(aspirant));
        
        
        
    }
    
     
    
    @Override
    public Aspirant generateUniqueObject(boolean isRemove) {
        Aspirant aspirant = generateAspirant(isRemove);
        return aspirant;
    }
    
}
