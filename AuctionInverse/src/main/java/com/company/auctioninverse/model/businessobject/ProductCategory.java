package com.company.auctioninverse.model.businessobject;

import com.company.common.model.businessobject.IObjectValue;
import com.company.common.model.businessobject.ObjectValue;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Class responsible of represent entity product category.
 *
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
@Entity
public class ProductCategory extends ObjectValue implements Serializable, IObjectValue {
    
    @Id
    @GeneratedValue
    private Integer id;
    @Column(name = "name", length = 100)
    private String name;
    @Column(name = "description", length = 60)
    private String description;
    @Column(name = "created_date", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "updated_date", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 43 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 43 * hash + (this.name != null ? this.name.hashCode() : 0);
        hash = 43 * hash + (this.description != null ? this.description.hashCode() : 0);
        hash = 43 * hash + (this.createdDate != null ? this.createdDate.hashCode() : 0);
        hash = 43 * hash + (this.updatedDate != null ? this.updatedDate.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProductCategory other = (ProductCategory) obj;
        if ((this.id == null) ? (other.id != null) : !this.id.equals(other.id)) {
            return false;
        }
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        if ((this.description == null) ? (other.description != null) : 
                !this.description.equals(other.description)) {
            return false;
        }
        if (this.createdDate != other.createdDate && (this.createdDate == null || 
                !this.createdDate.equals(other.createdDate))) {
            return false;
        }
        if (this.updatedDate != other.updatedDate && (this.updatedDate == null || 
                !this.updatedDate.equals(other.updatedDate))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ProductCategory{" + "id=" + name +", name=" + name + ", description=" + description 
                + ", createdDate=" + createdDate + ", updatedDate=" + updatedDate + '}';
    }
    
    
  
}
