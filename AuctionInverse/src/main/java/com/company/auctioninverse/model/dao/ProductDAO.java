package com.company.auctioninverse.model.dao;

import com.company.auctioninverse.model.businessobject.Product;

/**
 * Class that define contract for the product operations on SGBD.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
public interface ProductDAO extends GenericDAO<Product> {
    
}
