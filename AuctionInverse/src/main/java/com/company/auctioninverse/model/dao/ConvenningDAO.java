package com.company.auctioninverse.model.dao;

import com.company.auctioninverse.model.businessobject.Convenning;

/**
 * Class that define contract for the convenning operations on SGBD.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
public interface ConvenningDAO extends GenericDAO<Convenning> {
    
}
