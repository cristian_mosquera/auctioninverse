/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.company.auctioninverse.model.dao;

import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import com.mongodb.DBObject;

/**
 *
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Mosquera</a>
 */
public interface MongoDAO  {
    
    /**
     * Method responsible of save a object in mongo.
     * @param dBObject object to save
     * @throws AuctionInverseSystemException any system error
     */
    void save(DBObject dBObject) throws AuctionInverseSystemException;
    
    /**
     * Method responsible of update a object in mongo.
     * @param oldDBObject old value.
     * @param newDBObject new value
     * @throws AuctionInverseSystemException any system error.
     */
    void update(DBObject oldDBObject, DBObject newDBObject) throws AuctionInverseSystemException;
    
    /**
     * Method responsible of load a object in mongo.
     * @param oldDBObject object to load.
     * @return load a object in mongo.
     * @throws AuctionInverseSystemException any system error.
     */
    DBObject loadObject(DBObject oldDBObject) throws AuctionInverseSystemException;
}
