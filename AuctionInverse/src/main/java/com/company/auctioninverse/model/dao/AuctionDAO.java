package com.company.auctioninverse.model.dao;

import com.company.auctioninverse.model.businessobject.Admin;
import com.company.auctioninverse.model.businessobject.Aspirant;
import com.company.auctioninverse.model.businessobject.Auction;
import com.company.auctioninverse.model.businessobject.AuctionProduct;
import com.company.auctioninverse.model.businessobject.Inscribed;
import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import java.util.Date;
import java.util.List;

/**
 * Class that define contract for the auction operations on SGBD.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
public interface AuctionDAO extends GenericDAO<Auction> {
 
    /**
     * Method responsible of find auctions by admin an status.
     * @param admin admin for filter.
     * @param status status for filter
     * @throws AuctionInverseSystemException any system error.
     * @return List auctions by admin.
     */
    List<Auction> findAuctionsByAdminAndStatus(Admin admin, String status) 
            throws AuctionInverseSystemException; 
    
    /**
     * Method responsible of find auctions by admin.
     * @param admin admin for filter.
     * @param status status for filter
     * @param startDate start date for filter.
     * @param endDate end date for filter.
     * @param name name for filter.
     * @param aspirant aspirant to filter.
     * @throws AuctionInverseSystemException any system error.
     * @return List auctions by admin.
     */
    List<Auction> findAuctionsByAdmin(Admin admin, String status, 
            Date startDate, Date endDate, String name, Aspirant aspirant) 
            throws AuctionInverseSystemException; 
    
    /**
     * Method responsible of find auctions by inscribed.
     * @param inscribed inscribed for filter.
     * @param status status for filter
     * @param startDate start date for filter.
     * @param endDate end date for filter.
     * @param name name for filter.
     * @throws AuctionInverseSystemException any system error.
     * @return List auctions by admin.
     */
    List<Auction> findAuctionsByInscribed(Inscribed inscribed, String status, 
            Date startDate, Date endDate, String name) 
            throws AuctionInverseSystemException; 
    
    /**
     * Method responsible of find product by auction.
     * @param  auction auction for filter.
     * @return list of product by auction.
     * @throws AuctionInverseSystemException any system error.
     */
    List<AuctionProduct> findAuctionProductByAuction (Auction auction)
                                        throws AuctionInverseSystemException;
    
    /**
     * Method responsible of find winners by auction.
     * @param  auction auction for filter.
     * @return list of product by auction.
     * @throws AuctionInverseSystemException any system error.
     */
    List<Inscribed> findWinnersByAuction (Auction auction)
                                        throws AuctionInverseSystemException;
    
    
}
