package com.company.auctioninverse.model.service.impl;

import com.company.auctioninverse.model.businessobject.ProductCategory;
import com.company.auctioninverse.model.dao.ProductCategoryDAO;
import com.company.auctioninverse.model.service.ProductCategoryService;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service that implement contract for operations on product category entity.
 *
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
@Service(value = "productCategoryService")
public class ProductCategoryServiceImpl extends GenericServiceImpl<ProductCategory> 
                                                        implements ProductCategoryService {
    
    @Autowired
    private ProductCategoryDAO productCategoryDAO;
    
    
    /**
     * Method responsible of init class.
     */
    @PostConstruct
    public void init() {
        productCategoryDAO.setSessionFactory(sessionFactory);
        setGenericDao(productCategoryDAO);
    }
}
