package com.company.auctioninverse.model.filter;

import com.company.auctioninverse.model.businessobject.Person;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * Clase encargada de filtrar las paginas al login si no se a iniciado session
 *
 * @author Cristian Mosquera Puello <cmp166@hotmail.es>
 */
public class FilterUser implements Filter {

    private FilterConfig filterConfig = null;
    private ApplicationContext applicationContext;

    public FilterUser() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        applicationContext = WebApplicationContextUtils.
                getRequiredWebApplicationContext(request.getServletContext());
        String pathInfo = req.getPathInfo();
        String url = "/faces/login.xhtml";

        if (!pathInfo.startsWith("/login.xhtml") && pathInfo.indexOf("xhtml") != -1) {
        
            Person personLogged = (Person) req.getSession().
                    getAttribute("personLogged");
            if (personLogged == null) {
                request.getRequestDispatcher(url).forward(request, response);
            } else {
                chain.doFilter(request, response);
            }
        } else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {
    }

    @Override
    public void init(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }
}
