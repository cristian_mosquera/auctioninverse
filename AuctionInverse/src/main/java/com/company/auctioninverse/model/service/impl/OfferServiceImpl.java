package com.company.auctioninverse.model.service.impl;

import com.company.auctioninverse.model.businessobject.Offer;
import com.company.auctioninverse.model.dao.OfferDAO;
import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import com.company.auctioninverse.model.exceptions.AuctionInverseValidationException;
import com.company.auctioninverse.model.service.OfferService;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service that implement contract for operations on offer entity.
 *
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
@Service(value = "offerService")
public class OfferServiceImpl extends GenericServiceImpl<Offer> implements OfferService {
    
    @Autowired
    private OfferDAO offerDAO;
    
    
    /**
     * Method responsible of init class.
     */
    @PostConstruct
    public void init() {
        offerDAO.setSessionFactory(sessionFactory);
        setGenericDao(offerDAO);
    }
    
    @Transactional
    @Override
    public void save(Offer offer) throws AuctionInverseValidationException, 
                                                AuctionInverseSystemException {
        if (!"INPROGRESS".equals(offer.getRound().getStatus())) {
            throw  new AuctionInverseValidationException("La ronda debe estar en progreso");
        }
        super.save(offer);
    }
    
    
}
