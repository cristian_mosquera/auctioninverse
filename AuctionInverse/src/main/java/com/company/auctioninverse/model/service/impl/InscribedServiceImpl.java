package com.company.auctioninverse.model.service.impl;

import com.company.auctioninverse.model.businessobject.Auction;
import com.company.auctioninverse.model.businessobject.Inscribed;
import com.company.auctioninverse.model.dao.InscribedDAO;
import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import com.company.auctioninverse.model.exceptions.AuctionInverseValidationException;
import com.company.auctioninverse.model.service.InscribedService;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service that implement contract for operations on inscribed entity.
 *
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
@Service(value = "inscribedService")
public class InscribedServiceImpl extends GenericServiceImpl<Inscribed> 
                                                    implements InscribedService {
    
    @Autowired
    private InscribedDAO inscribedDAO;
    
    
    /**
     * Method responsible of init class.
     */
    @PostConstruct
    public void init() {
        inscribedDAO.setSessionFactory(sessionFactory);
        setGenericDao(inscribedDAO);
    }

    @Override
    public List<Inscribed> findInscribedByAuction(Auction auction) 
            throws AuctionInverseSystemException, AuctionInverseValidationException {
        try {
            return inscribedDAO.findInscribedByAuction(auction);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new AuctionInverseSystemException(e.getMessage(), e);
        }
       
    }
}
