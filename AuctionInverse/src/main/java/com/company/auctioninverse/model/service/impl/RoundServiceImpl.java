package com.company.auctioninverse.model.service.impl;

import com.company.auctioninverse.model.businessobject.Auction;
import com.company.auctioninverse.model.businessobject.Inscribed;
import com.company.auctioninverse.model.businessobject.Offer;
import com.company.auctioninverse.model.businessobject.Round;
import com.company.auctioninverse.model.dao.MongoDAO;
import com.company.auctioninverse.model.dao.RoundDAO;
import com.company.auctioninverse.model.dao.impl.MongoDAOImpl;
import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import com.company.auctioninverse.model.exceptions.AuctionInverseValidationException;
import com.company.auctioninverse.model.service.AuctionService;
import com.company.auctioninverse.model.service.RoundService;
import com.company.auctioninverse.model.thread.RoundTimerTask;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import java.util.List;
import java.util.Timer;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service that implement contract for operations on round entity.
 *
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
@Service(value = "roundService")
public class RoundServiceImpl extends GenericServiceImpl<Round> implements RoundService {
    
    @Autowired
    private RoundDAO roundDAO;
    
    @Autowired
    private AuctionService auctionService;
    
    
    /**
     * Method responsible of init class.
     */
    @PostConstruct
    public void init() {
        roundDAO.setSessionFactory(sessionFactory);
        setGenericDao(roundDAO);
    }

    @Override
    public List<Round> findRoundsByAuction(Auction auction) throws AuctionInverseSystemException {
        try {
            List<Round> rounds = roundDAO.findRoundsByAuction(auction);
            for (Round round : rounds) {
                loadTimeTranscurredByRound(round);
            }
            return rounds;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new AuctionInverseSystemException(e.getMessage(), e);
        }
    }

    @Override
    public List<Inscribed> findWinnerByRound(Round round) throws AuctionInverseSystemException, 
                                                                 AuctionInverseValidationException {
         try {
            List<Inscribed> inscribeds = roundDAO.findWinnerByRound(round);
            return inscribeds;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new AuctionInverseSystemException(e.getMessage(), e);
        }
    }

    @Override
    public List<Offer> findOffersByRound(Round round) throws AuctionInverseSystemException, 
            AuctionInverseValidationException {
         try {
            List<Offer> offers = roundDAO.findOffersByRound(round);
            return offers;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new AuctionInverseSystemException(e.getMessage(), e);
        }
    }

    @Override
    public List<Offer> findOfferByRoundInscribed(Round round, Inscribed inscribed) 
            throws AuctionInverseSystemException, AuctionInverseValidationException {
        try {
            List<Offer> offers = roundDAO.findOfferByRoundInscribed(round, inscribed);
            return offers;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new AuctionInverseSystemException(e.getMessage(), e);
        }
    }

    @Override
    public Round loadCurrentRoundByAuction(Auction auction) 
            throws AuctionInverseSystemException, AuctionInverseValidationException {
        try {
            Round round = roundDAO.loadCurrentRoundByAuction(auction);
            loadTimeTranscurredByRound(round);
            return round;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new AuctionInverseSystemException(e.getMessage(), e);
        }
    }

    @Override
    public void loadTimeTranscurredByRound(Round round) 
            throws AuctionInverseSystemException, AuctionInverseValidationException {
        try {
            if (round != null) {
                MongoDAO mongoDAO = new MongoDAOImpl();
                DBObject obj = new BasicDBObject();
                obj.put("id", round.getId());
                DBObject objFind = mongoDAO.loadObject(obj);
                if (objFind != null) {
                    round.setTimeTranscurred(objFind.get("timeTranscurred").toString());
                }
            }

        }
        catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new AuctionInverseSystemException(e.getMessage(), e);
        }
    }

    @Override
    public void startTimeRound(Round round)
            throws AuctionInverseSystemException, AuctionInverseValidationException {
        final Timer timer = new Timer();
        timer.schedule(new RoundTimerTask(round, timer, auctionService), 0, 700);
    }

    @Override
    public Integer quantityRoundByAuction(Auction auction) 
            throws AuctionInverseSystemException, AuctionInverseValidationException {
        try {
            Integer quantity = roundDAO.quantityRoundByAuction(auction);
            return quantity;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new AuctionInverseSystemException(e.getMessage(), e);
        }
    }
}
