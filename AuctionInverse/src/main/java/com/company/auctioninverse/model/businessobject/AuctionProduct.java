/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.company.auctioninverse.model.businessobject;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

/**
 * Class responsible of represent entity auction product
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Mosquera</a>
 */
@Entity
public class AuctionProduct extends ProductRelation {
    
    
    private Auction auction;
    
    @Cascade(CascadeType.SAVE_UPDATE)
    @ManyToOne
    @JoinColumn(name = "auction_id", nullable = false)
    public Auction getAuction() {
        return auction;
    }

    public void setAuction(Auction auction) {
        this.auction = auction;
    }
    
    @Override
    public String toString() {
        return super.toString() + "AuctionProduct{" + "auction=" + auction + '}';
    }
    
    
    
    
    
}
