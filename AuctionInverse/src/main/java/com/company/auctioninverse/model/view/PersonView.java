package com.company.auctioninverse.model.view;

import com.company.auctioninverse.model.businessobject.Admin;
import com.company.auctioninverse.model.businessobject.Inscribed;
import com.company.auctioninverse.model.businessobject.Person;
import com.company.auctioninverse.model.exceptions.AuctionInverseValidationException;
import com.company.auctioninverse.model.service.AdminService;
import com.company.auctioninverse.model.service.InscribedService;
import com.company.auctioninverse.model.service.PersonService;
import com.company.auctioninverse.model.service.impl.ServiceLocator;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 * Class responsible of handle events person.
 *
 * @author <a href="mailto:mosquerapuello@gmail.com"> Cristian Jose Mosquera Puello</a>
 */
@SessionScoped
@ManagedBean
public class PersonView implements Serializable {

    private String user;
    
    private String password;
    
    private PersonService personService;
    
    private InscribedService inscribedService;
    
    private AdminService adminService;
    
    private Person personLogged;
    
    @PostConstruct
    public void init() {
        loadService();
    }
    
    public void loadService() {
        adminService = (AdminService) 
                                ServiceLocator.getService("adminService");
        inscribedService = (InscribedService) 
                                ServiceLocator.getService("inscribedService");                        
        personService = (PersonService) ServiceLocator.getService
                                ("personService");
    }
    
    public String login() {
        String rule = "";
        try {
            personLogged = personService.login(user, password);
            if (personLogged != null) {
                rule = "login";
                if ("Admin".equals(personLogged.getRol().getName())) {
                   personLogged = adminService.find(personLogged.getId(), Admin.class);
                }
                else if ("Inscribed".equals(personLogged.getRol().getName())) {
                    personLogged = inscribedService.find(personLogged.getId(), Inscribed.class);
                }
                
                
                HttpSession session = (HttpSession) FacesContext.getCurrentInstance().
                        getExternalContext().getSession(false);
                session.setAttribute("personLogged", personLogged);
                user = null;
                password = null;
            }

        } catch (AuctionInverseValidationException e) {
            FacesContext.getCurrentInstance().addMessage
                      (null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    e.getMessage(), ""));
            user = null;
            password = null;
            return "faillogin";
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage
                        (null, new FacesMessage(FacesMessage.SEVERITY_FATAL,
                    "Ocurrio un error en la aplicacion consulte al "
                    + "administrador del sistema", ""));
            e.printStackTrace();
        }
        return rule;
    }

    public String logOut() {
        user = null;
        password = null;
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().
                getExternalContext().getSession(false);
        session.removeAttribute("personLogged");
        return "clseSession";
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Person getPersonLogged() {
        return personLogged;
    }

    public void setPersonLogged(Person personLogged) {
        this.personLogged = personLogged;
    }
    
}
