package com.company.auctioninverse.model.service.impl;

import com.company.auctioninverse.model.businessobject.Product;
import com.company.auctioninverse.model.dao.ProductDAO;
import com.company.auctioninverse.model.service.ProductService;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service that implement contract for operations on product entity.
 *
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
@Service(value = "productService")
public class ProductServiceImpl extends GenericServiceImpl<Product> implements ProductService {
    
    @Autowired
    private ProductDAO productDAO;
    
    
    /**
     * Method responsible of init class.
     */
    @PostConstruct
    public void init() {
        productDAO.setSessionFactory(sessionFactory);
        setGenericDao(productDAO);
    }
}
