

package com.company.auctioninverse.model.service;

import com.company.auctioninverse.model.businessobject.Admin;
import com.company.auctioninverse.model.businessobject.Aspirant;
import com.company.auctioninverse.model.businessobject.Auction;
import com.company.auctioninverse.model.businessobject.AuctionProduct;
import com.company.auctioninverse.model.businessobject.Inscribed;
import com.company.auctioninverse.model.businessobject.Offer;
import com.company.auctioninverse.model.businessobject.Round;
import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import com.company.auctioninverse.model.exceptions.AuctionInverseValidationException;
import java.util.Date;
import java.util.List;

/**
 * Service that define contract for operations on auction entity.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
public interface AuctionService extends GenericService<Auction>{

    /**
     * Method responsible of find auctions by admin.
     * @param admin admin for filter.
     * @param status status for filter
     * @param startDate start date for filter.
     * @param endDate end date for filter.
     * @param name name for filter.
     * @param aspirant aspirant to filter.
     * @throws AuctionInverseSystemException any system error.
     * @throws AuctionInverseValidationException any validation error.
     * @return List auctions by admin.
     */
    List<Auction> findAuctionsByAdmin(Admin admin, String status, 
            Date startDate, Date endDate, String name, Aspirant aspirant) 
            throws AuctionInverseSystemException, AuctionInverseValidationException; 
    
    /**
     * Method responsible of find auctions by inscribed.
     * @param inscribed inscribed for filter.
     * @param status status for filter
     * @param startDate start date for filter.
     * @param endDate end date for filter.
     * @param name name for filter.
     * @throws AuctionInverseSystemException any system error.
     * @throws AuctionInverseValidationException any validation error.
     * @return List auctions by inscribed.
     */
    List<Auction> findAuctionsByInscribed(Inscribed inscribed, String status, 
            Date startDate, Date endDate, String name) 
            throws AuctionInverseSystemException, AuctionInverseValidationException; 
    
    /**
     * Method responsible of find product by auction.
     * @param  auction auction for filter.
     * @return list of product by auction.
     * @throws AuctionInverseSystemException any system error.
     * @throws AuctionInverseValidationException any validation erro;
     */
    List<AuctionProduct> findAuctionProductByAuction (Auction auction)
                          throws AuctionInverseSystemException, AuctionInverseValidationException;
    
    /**
     * Method responsible of find winners by auction.
     * @param  auction auction for filter.
     * @return list of product by auction.
     * @throws AuctionInverseSystemException any system error.
     * @throws AuctionInverseValidationException any validation erro;
     */
    List<Inscribed> findWinnersByAuction (Auction auction)
                         throws AuctionInverseSystemException, AuctionInverseValidationException;
    
    /**
     * Method responsible of start auction.
     * @param auction auction to start.
     * @throws AuctionInverseSystemException any system error.
     * @throws AuctionInverseValidationException any validation error. 
     */
    void startAuction(Auction auction) 
            throws AuctionInverseSystemException, AuctionInverseValidationException;
    
    /**
     * Method responsible of save offer.
     * @param offer auction to save.
     * @throws AuctionInverseSystemException any system error.
     * @throws AuctionInverseValidationException any validation error. 
     */
    void saveOffer(Offer offer) 
            throws AuctionInverseSystemException, AuctionInverseValidationException;
    
    /**
     * Method responsible of create new round.
     * @param round round to create.
     * @throws AuctionInverseSystemException any system error.
     * @throws AuctionInverseValidationException  any validation error.
     */
    void newRound(Round round) 
            throws AuctionInverseSystemException, AuctionInverseValidationException;
}
