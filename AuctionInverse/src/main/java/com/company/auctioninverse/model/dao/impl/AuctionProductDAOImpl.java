package com.company.auctioninverse.model.dao.impl;

import com.company.auctioninverse.model.businessobject.AuctionProduct;
import com.company.auctioninverse.model.dao.AuctionProductDAO;
import org.springframework.stereotype.Repository;

/**
 * Class that implement contract for the auction product operations  on SGBD. 
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
@Repository
public class AuctionProductDAOImpl extends GenericDAOImpl<AuctionProduct> 
                                                            implements AuctionProductDAO {
    

}
