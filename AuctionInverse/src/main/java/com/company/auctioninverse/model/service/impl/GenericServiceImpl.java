
package com.company.auctioninverse.model.service.impl;

import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import com.company.auctioninverse.model.exceptions.AuctionInverseValidationException;
import com.company.auctioninverse.model.service.GenericService;

/**
 * Service that implement the general operations  on SGBD.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 * @param <K> Entity relacionared.
 */

public abstract class GenericServiceImpl<K> 
    extends com.company.common.model.generic.service.impl.GenericServiceImpl
        <K, AuctionInverseSystemException, AuctionInverseValidationException> 
                                                            implements GenericService<K> {
    
   
}
