package com.company.auctioninverse.model.service.impl;

import com.company.auctioninverse.model.businessobject.Admin;
import com.company.auctioninverse.model.businessobject.Aspirant;
import com.company.auctioninverse.model.businessobject.Auction;
import com.company.auctioninverse.model.businessobject.AuctionProduct;
import com.company.auctioninverse.model.businessobject.Inscribed;
import com.company.auctioninverse.model.businessobject.Offer;
import com.company.auctioninverse.model.businessobject.Round;
import com.company.auctioninverse.model.dao.impl.AuctionDAOImpl;
import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import com.company.auctioninverse.model.exceptions.AuctionInverseValidationException;
import com.company.auctioninverse.model.service.AuctionService;
import com.company.auctioninverse.model.service.InscribedService;
import com.company.auctioninverse.model.service.OfferService;
import com.company.auctioninverse.model.service.RoundService;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service that implement contract for operations on auction entity.
 *
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
@Service(value = "auctionService")
public class AuctionServiceImpl extends GenericServiceImpl<Auction> implements AuctionService {
    
    @Autowired
    private AuctionDAOImpl auctionDAO;
    
    @Autowired
    private RoundService roundService;
    
    @Autowired
    private InscribedService inscribedService;
    
    @Autowired
    private OfferService offerService;
    
    
    
    /**
     * Method responsible of init class.
     */
    @PostConstruct
    public void init() {
        auctionDAO.setSessionFactory(sessionFactory);
        setGenericDao(auctionDAO);
    }

    @Override
    public List<Auction> findAuctionsByAdmin(Admin admin, String status, Date startDate, 
            Date endDate, String name, Aspirant aspirant) 
            throws AuctionInverseSystemException, AuctionInverseValidationException {
        try {
            List<Auction> auctions = auctionDAO.findAuctionsByAdmin
                            (admin, status, startDate, endDate, name, aspirant);
            return auctions;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new AuctionInverseSystemException(e.getMessage(), e);
        }
        
    }

    @Override
    public List<Auction> findAuctionsByInscribed(Inscribed inscribed, String status, 
            Date startDate, Date endDate, String name) 
            throws AuctionInverseSystemException, AuctionInverseValidationException {
        try {
            return auctionDAO.findAuctionsByInscribed(inscribed, status, startDate, endDate, name);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new AuctionInverseSystemException(e.getMessage(), e);
        }
        
    }

    @Override
    public List<AuctionProduct> findAuctionProductByAuction(Auction auction) 
            throws AuctionInverseSystemException, AuctionInverseValidationException {
        try {
            List<AuctionProduct> auctionProducts = auctionDAO.findAuctionProductByAuction(auction);
            return auctionProducts;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new AuctionInverseSystemException(e.getMessage(), e);
        }
        
    }

    @Override
    public List<Inscribed> findWinnersByAuction(Auction auction) 
            throws AuctionInverseSystemException, AuctionInverseValidationException {
        try {
            List<Inscribed> winners = auctionDAO.findWinnersByAuction(auction);
            return winners;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new AuctionInverseSystemException(e.getMessage(), e);
        }
    }

    @Transactional
    @Override
    public void startAuction(Auction auction)
            throws AuctionInverseSystemException, AuctionInverseValidationException {
        try {
            Auction auctionFind = auctionDAO.find(auction.getId(), Auction.class);
            auctionFind.setStatus(auction.getStatus());
            update(auctionFind);
            Round round = new Round();
            round.setAuction(auctionFind);
            round.setCurrent(Boolean.TRUE);
            List<Inscribed> inscribeds = inscribedService.findInscribedByAuction(auctionFind);
            round.setInscribeds(inscribeds);
            round.setNumber(new Short("1"));
            round.setStatus("INPROGRESS");
            roundService.save(round);
            roundService.startTimeRound(round);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new AuctionInverseSystemException(e.getMessage(), e);
        }

    }
    
    @Transactional
    @Override
    public void saveOffer(Offer offer) 
            throws AuctionInverseSystemException, AuctionInverseValidationException {
        try {
            Round roundFind = roundService.find(offer.getRound().getId(), Round.class);
            Inscribed inscribed = inscribedService.
                                find(offer.getInscribed().getId(), Inscribed.class);
            offer.setRound(roundFind);
            offer.setInscribed(inscribed);
            offerService.save(offer);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new AuctionInverseSystemException(e.getMessage(), e);
        }
    
    } 
    
    @Transactional
    public void newRound(Round round) 
            throws AuctionInverseSystemException, AuctionInverseValidationException {
        try {
            Auction auctionFind = auctionDAO.find(round.getAuction().getId(), Auction.class);
            int roundQuantity = roundService.quantityRoundByAuction(auctionFind).intValue();
            round.setAuction(auctionFind);
            round.setCurrent(Boolean.TRUE);
            List<Inscribed> inscribeds = inscribedService.findInscribedByAuction(auctionFind);
            round.setInscribeds(inscribeds);
            round.setNumber(new Short("" + (roundQuantity + 1)));
            round.setStatus("INPROGRESS");
            List<Round> rounds = roundService.findRoundsByAuction(auctionFind);
            for (Round round1 : rounds) {
                round1.setStatus("CLOSED");
                round1.setCurrent(Boolean.FALSE);
                roundService.update(round1);
            }
            if (roundQuantity < 5) {
              roundService.save(round);
              roundService.startTimeRound(round);  
            }
            
        } 
        catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new AuctionInverseSystemException(e.getMessage(), e);
        }
        
    }
    
}
