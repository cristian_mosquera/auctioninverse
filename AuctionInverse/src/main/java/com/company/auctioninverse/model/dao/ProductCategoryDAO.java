package com.company.auctioninverse.model.dao;

import com.company.auctioninverse.model.businessobject.ProductCategory;

/**
 * Class that define contract for the product category operations on SGBD.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
public interface ProductCategoryDAO extends GenericDAO<ProductCategory> {
    
}
