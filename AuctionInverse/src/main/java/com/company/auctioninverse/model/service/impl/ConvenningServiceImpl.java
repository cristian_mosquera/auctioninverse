package com.company.auctioninverse.model.service.impl;

import com.company.auctioninverse.model.businessobject.Convenning;
import com.company.auctioninverse.model.dao.ConvenningDAO;
import com.company.auctioninverse.model.service.ConvenningService;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service that implement contract for operations on convenning entity.
 *
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
@Service(value = "convenningService")
public class ConvenningServiceImpl extends GenericServiceImpl<Convenning> 
                                                        implements ConvenningService {
    
    @Autowired
    private ConvenningDAO convenningDAO;
    
    
    /**
     * Method responsible of init class.
     */
    @PostConstruct
    public void init() {
        convenningDAO.setSessionFactory(sessionFactory);
        setGenericDao(convenningDAO);
    }
}
