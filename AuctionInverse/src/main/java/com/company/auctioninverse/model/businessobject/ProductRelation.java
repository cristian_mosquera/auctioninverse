/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.auctioninverse.model.businessobject;

import com.company.common.model.businessobject.ObjectValue;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

/**
 * Class responsible of represent entity relation product
 *
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Mosquera</a>
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "AuctionRelation", discriminatorType = DiscriminatorType.STRING)
public class ProductRelation extends ObjectValue implements Serializable {
    
    private Long id;

    private Product product;
    private Integer quantity;
    private Double price;
    
    
    @Override
    @GeneratedValue
    @Id
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Cascade(CascadeType.SAVE_UPDATE)
    @ManyToOne
    @JoinColumn(name = "product_id", nullable = false)
    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Column(length = 10, nullable = false, name = "quantity")
    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Column(nullable = false, precision = 10, scale = 2, name = "price")
    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Transient
    public Double getSubTotal() {
        if (price != null && !price.isNaN() && quantity != null) {
            return price * quantity;
        }
        return 0.0;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 29 * hash + (this.product != null ? this.product.hashCode() : 0);
        hash = 29 * hash + (this.quantity != null ? this.quantity.hashCode() : 0);
        hash = 29 * hash + (this.price != null ? this.price.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProductRelation other = (ProductRelation) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        if (this.product != other.product && (this.product == null
                || !this.product.equals(other.product))) {
            return false;
        }
        if (this.quantity != other.quantity && (this.quantity == null
                || !this.quantity.equals(other.quantity))) {
            return false;
        }
        if (this.price != other.price && (this.price == null || !this.price.equals(other.price))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AuctionProduct{" + "id=" + id + ", product=" + product
                + ", quantity=" + quantity + ", price=" + price + '}';
    }

}
