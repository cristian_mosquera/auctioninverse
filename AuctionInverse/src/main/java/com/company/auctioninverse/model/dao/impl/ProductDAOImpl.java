package com.company.auctioninverse.model.dao.impl;

import com.company.auctioninverse.model.businessobject.Product;
import com.company.auctioninverse.model.dao.ProductDAO;
import org.springframework.stereotype.Repository;

/**
 * Class that implement contract for the product operations  on SGBD. 
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
@Repository
public class ProductDAOImpl extends GenericDAOImpl<Product> implements ProductDAO {
    
}
