package com.company.auctioninverse.model.dao;

import com.company.auctioninverse.model.businessobject.Rol;

/**
 * Class that define contract for the rol operations on SGBD.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
public interface RolDAO extends GenericDAO<Rol> {
    
}
