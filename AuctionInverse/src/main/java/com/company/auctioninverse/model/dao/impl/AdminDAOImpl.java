package com.company.auctioninverse.model.dao.impl;

import com.company.auctioninverse.model.businessobject.Admin;
import com.company.auctioninverse.model.dao.AdminDAO;
import org.springframework.stereotype.Repository;

/**
 * Class that implement contract for the admin operations  on SGBD. 
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
@Repository
public class AdminDAOImpl extends GenericDAOImpl<Admin> implements AdminDAO {

}
