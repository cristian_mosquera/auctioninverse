

package com.company.auctioninverse.model.service;

import com.company.auctioninverse.model.businessobject.Auction;
import com.company.auctioninverse.model.businessobject.Inscribed;
import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import com.company.auctioninverse.model.exceptions.AuctionInverseValidationException;
import java.util.List;

/**
 * Service that define contract for operations on inscribed entity.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
public interface InscribedService extends GenericService<Inscribed>{
    
    /**
     * Method responsible of find inscribed by auction.
     * @param auction auction for filter.
     * @return list of inscribed by auction.
     * @throws AuctionInverseSystemException any system error
     */
    List <Inscribed> findInscribedByAuction(Auction auction) throws AuctionInverseSystemException,
            AuctionInverseValidationException;
}
