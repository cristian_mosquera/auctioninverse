package com.company.auctioninverse.model.dao;

import com.company.auctioninverse.model.businessobject.AuctionProduct;

/**
 * Class that define contract for the auction inscribed operations on SGBD.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
public interface AuctionInscribedDAO extends GenericDAO<AuctionProduct> {
    
    
    
    
}
