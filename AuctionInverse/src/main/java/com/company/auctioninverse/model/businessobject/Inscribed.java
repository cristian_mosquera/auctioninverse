package com.company.auctioninverse.model.businessobject;

import java.util.List;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

/**
 * Class responsible of represent entity inscribed.
 *
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
@DiscriminatorValue("INSCRIBED")
@Entity
public class Inscribed extends Person {
    
    
    @Cascade({CascadeType.SAVE_UPDATE, CascadeType.DELETE})
    @OneToMany(mappedBy = "inscribed")
    private List<Offer> offers;
    @Cascade(CascadeType.SAVE_UPDATE)
    @OneToOne
    @JoinColumn(name = "aspirant_id", nullable = false)
    private Aspirant aspirant;
    @OneToMany(mappedBy = "inscribed")
    @Cascade(CascadeType.ALL)
    private List<InscribedProduct> inscribedProducts;
    
   
    public void setOffers(List<Offer> offers) {
        this.offers = offers;
    }

    public void setAspirant(Aspirant aspirant) {
        this.aspirant = aspirant;
    }

    public List<InscribedProduct> getInscribedProducts() {
        return inscribedProducts;
    }

    public void setInscribedProducts(List<InscribedProduct> inscribedProducts) {
        this.inscribedProducts = inscribedProducts;
    }

    public Aspirant getAspirant() {
        return aspirant;
    }

    public List<Offer> getOffers() {
        return offers;
    }
    
    
    
    

    
    @Override
    public String toString() {
        return super.toString() + "Inscribed{" + ", aspirant=" 
                + aspirant  + '}';
    }
    
    
    
}
