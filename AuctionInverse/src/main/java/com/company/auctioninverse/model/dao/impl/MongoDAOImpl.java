
package com.company.auctioninverse.model.dao.impl;

import com.company.auctioninverse.model.dao.MongoDAO;
import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

/**
 *
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Mosquera</a>
 */
public class MongoDAOImpl implements MongoDAO {

    
    private static MongoClient mongo;
    @Override
    public void save(DBObject dBObject) throws AuctionInverseSystemException {
        try {
            DB db = getMongoClient().getDB("clientes");
            DBCollection collection = db.getCollection("clientes");
            collection.insert(dBObject);
           
        } 
        catch (Exception e) {
            throw new AuctionInverseSystemException(e.getMessage(), e);
        }
        finally {
            mongo.close();
        }
    }

    @Override
    public void update(DBObject oldDBObject, DBObject newDBObject) 
                                        throws AuctionInverseSystemException {
        try {
            DB db = getMongoClient().getDB("clientes");
            DBCollection collection = db.getCollection("clientes");
            collection.update(oldDBObject, newDBObject);
           
        }
        catch (Exception e) {
            throw new AuctionInverseSystemException(e.getMessage(), e);
        }
        finally {
            mongo.close();
        }
    }

    @Override
    public DBObject loadObject(DBObject oldDBObject) throws AuctionInverseSystemException {
        try {
            DB db = getMongoClient().getDB("clientes");
            DBCollection collection = db.getCollection("clientes");
            DBCursor dBCursor = collection.find(oldDBObject);
            if (dBCursor.hasNext()) {
                return dBCursor.next();
            } else {
                return null;
            }

        } catch (Exception e) {
            throw new AuctionInverseSystemException(e.getMessage(), e);
        } 
        finally {
            mongo.close();
        }

    }
    
    public static MongoClient getMongoClient() throws AuctionInverseSystemException {
        try {
            mongo = new MongoClient( "localhost" , 27017 );
            return mongo;
        } catch (Exception e) {
            throw new AuctionInverseSystemException(e.getMessage(), e);
        }
    }
}
