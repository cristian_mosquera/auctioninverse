package com.company.auctioninverse.model.dao.impl;

import com.company.auctioninverse.model.businessobject.Auction;
import com.company.auctioninverse.model.businessobject.Inscribed;
import com.company.auctioninverse.model.businessobject.Offer;
import com.company.auctioninverse.model.businessobject.Round;
import com.company.auctioninverse.model.dao.RoundDAO;
import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;

/**
 * Class that implement contract for the round operations  on SGBD. 
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
@Repository
public class RoundDAOImpl extends GenericDAOImpl<Round> implements RoundDAO {

    @Override
    public List<Round> findRoundsByAuction(Auction auction) throws AuctionInverseSystemException {
        List parameters = new ArrayList();
         String sql = "select o from " + Round.class.getSimpleName() + " o ";
         sql+= " left join fetch o.inscribeds inscribeds";
         if (auction != null && auction.getId() != null) {
            parameters.add(auction.getId());
            sql += " where o.auction.id = ?";
        }
        return getHibernateTemplate().find(sql, parameters.toArray());
    }
    
    @Override
    public List<Inscribed> findWinnerByRound(Round round) throws AuctionInverseSystemException {
        List parameters = new ArrayList();
         String sql = "select o.winners from " + Round.class.getSimpleName() + " o ";
         if (round != null && round.getId() != null) {
            parameters.add(round.getId());
            sql += " where o.id = ?";
        }
        return getHibernateTemplate().find(sql, parameters.toArray());
    }
    
    @Override
    public List<Offer> findOffersByRound(Round round) throws AuctionInverseSystemException {
        List parameters = new ArrayList();
        String sql = "select offers from " + Round.class.getSimpleName() + " o ";
        sql += " join o.offers offers";
        if (round != null && round.getId() != null) {
            parameters.add(round.getId());
            sql += " where o.id = ?";
        }
        return getHibernateTemplate().find(sql, parameters.toArray());
    }

    @Override
    public List<Offer> findOfferByRoundInscribed(Round round, Inscribed inscribed)
            throws AuctionInverseSystemException {
        List parameters = new ArrayList();
        String sql = "select offers from " + Round.class.getSimpleName() + " o ";
        sql += " join o.offers offers";
        if (round != null && round.getId() != null) {
            parameters.add(round.getId());
            sql += " where o.id = ?";
        }
        if (inscribed != null && inscribed.getId() != null) {
            parameters.add(inscribed.getId());
             if (sql.contains("where")) {
                sql += " and offers.inscribed.id = ?";
            } else {
                sql += " where offers.inscribed.id = ?";
            }
            
        }
        return getHibernateTemplate().find(sql, parameters.toArray());
    }

    @Override
    public Round loadCurrentRoundByAuction(Auction auction) throws AuctionInverseSystemException {
        List parameters = new ArrayList();
        String sql = "select o from " + Round.class.getSimpleName() + " o ";
        if (auction != null && auction.getId() != null) {
            parameters.add(auction.getId());
            parameters.add(Boolean.TRUE);
            sql += " where o.auction.id = ? and o.current = ? ";
        }
        Round round = null;
        Iterator <Round> iterator = getHibernateTemplate().
                find(sql, parameters.toArray()).iterator();
        if (iterator.hasNext()) {
            round = iterator.next();
        }
        return round;
    }
    
    @Override
    public Integer quantityRoundByAuction(final Auction auction) 
            throws AuctionInverseSystemException {
        List parameters = new ArrayList();
        final StringBuilder builder = new StringBuilder();
        builder.append("select count(o) from " + Round.class.getSimpleName() + " o ");
        if (auction != null && auction.getId() != null) {
            parameters.add(auction.getId());
            builder.append(" where o.auction.id = :auctionId  ");
        }
        Long result = 
        getHibernateTemplate().execute(new HibernateCallback<Long> () {

            @Override
            public Long doInHibernate(Session sn) throws HibernateException, SQLException {
                Query query = sn.createQuery(builder.toString());
                query.setLong("auctionId", auction.getId());
                return (Long) query.uniqueResult();
            }
        });
        
        
        return result.intValue();
    }
    
}
