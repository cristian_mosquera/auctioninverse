package com.company.auctioninverse.model.service.impl;

import com.company.auctioninverse.model.businessobject.AuctionProduct;
import com.company.auctioninverse.model.dao.AuctionProductDAO;
import com.company.auctioninverse.model.service.AuctionProductService;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service that implement contract for operations on admin entity.
 *
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
@Service(value = "auctionProductService")
public class AuctionProductServiceImpl extends GenericServiceImpl<AuctionProduct> 
                                                            implements AuctionProductService {
    
    @Autowired
    private AuctionProductDAO auctionProductDAO;
    
    
    /**
     * Method responsible of init class.
     */
    @PostConstruct
    public void init() {
        auctionProductDAO.setSessionFactory(sessionFactory);
        setGenericDao(auctionProductDAO);
    }    
}
