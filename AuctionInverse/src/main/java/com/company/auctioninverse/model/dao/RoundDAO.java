package com.company.auctioninverse.model.dao;

import com.company.auctioninverse.model.businessobject.Auction;
import com.company.auctioninverse.model.businessobject.Inscribed;
import com.company.auctioninverse.model.businessobject.Offer;
import com.company.auctioninverse.model.businessobject.Round;
import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import com.company.auctioninverse.model.exceptions.AuctionInverseValidationException;
import java.util.List;

/**
 * Class that define contract for the round operations on SGBD.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
public interface RoundDAO extends GenericDAO<Round> {
    
    /**
     * Method responsible of find rounds by auction.
     * @param auction auction for filter.
     * @return list of rounds by auction.
     * @throws AuctionInverseSystemException any system error.
     */
    List<Round> findRoundsByAuction (Auction auction) throws AuctionInverseSystemException;
    
    /**
     * Method responsible of find winners by round.
     * @param round round for filter.
     * @return list of winners by round.
     * @throws AuctionInverseSystemException any system error.
     */
    List<Inscribed> findWinnerByRound(Round round) throws AuctionInverseSystemException;
    
    /**
     * Method responsible of find offers by round.
     * @param round round for filter.
     * @return list of offers by round.
     * @throws AuctionInverseSystemException any system error.
     */
    List<Offer> findOffersByRound(Round round) throws AuctionInverseSystemException;
    
    /**
     * Method responsible of find offers by round.
     * @param round round for filter.
     * @param inscribed inscribed for filter.
     * @return list of offers by round.
     * @throws AuctionInverseSystemException any system error.
     */
    List<Offer> findOfferByRoundInscribed(Round round, Inscribed inscribed) 
            throws AuctionInverseSystemException;
    
    /**
     * Method responsible of load current round by auction.
     * @param auction auction to filter.
     * @return current round by auction.
     * @throws AuctionInverseSystemException any system error.
     */
    Round loadCurrentRoundByAuction(Auction auction) throws AuctionInverseSystemException;
    
    /**
     * Method responsible of load quantity round by auction.
     * @param auction auction to filter.
     * @return uantity round by auction.
     * @throws AuctionInverseSystemException any system error.
     */
    Integer quantityRoundByAuction(Auction auction) throws AuctionInverseSystemException;
}
