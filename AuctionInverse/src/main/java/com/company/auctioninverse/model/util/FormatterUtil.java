package com.company.auctioninverse.model.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Class responsible of handle utilities for formatter.
 *
 * @author <a href="mailto:mosquerapuello@gmail.com"> Cristian Jose Mosquera Puello</a>
 */
public class FormatterUtil {
    
    private static final int FIRST_HOUR_OF_DAY = 0;
    private static final int FIRST_MINUTE_OF_DAY = 0;
    private static final int FIRST_SECOND_OF_DAY = 0;
    private static final int LAST_HOUR_OF_DAY = 23;
    private static final int LAST_MINUTE_OF_DAY = 59;
    private static final int LAST_SECOND_OF_DAY = 59;
    private static final int LAST_MILISECOND_OF_DAY = 999;
    /**
     * Returns date with hour format 00:00:00:00.
     *
     * @param date to convert.
     * @return date with hour format 00:00:00:00.
     */
    public static Date getDateWithStartHourOfDay(Date date) {
        Calendar dateStartHour = Calendar.getInstance();
        dateStartHour.setTime(date);
        dateStartHour.set(Calendar.HOUR_OF_DAY, FIRST_HOUR_OF_DAY);
        dateStartHour.set(Calendar.MINUTE, FIRST_MINUTE_OF_DAY);
        dateStartHour.set(Calendar.SECOND, FIRST_SECOND_OF_DAY);
        dateStartHour.set(Calendar.MILLISECOND, FIRST_SECOND_OF_DAY);
        return dateStartHour.getTime();
    }

    /**
     * Returns date with hour format 23:59:59:999.
     *
     * @param date to convert.
     * @return date with hour format 23:59:59:999.
     */
    public static Date getDateWithEndHourOfDay(Date date) {
        Calendar dateEndHour = Calendar.getInstance();
        dateEndHour.setTime(date);
        dateEndHour.set(Calendar.HOUR_OF_DAY, LAST_HOUR_OF_DAY);
        dateEndHour.set(Calendar.MINUTE, LAST_MINUTE_OF_DAY);
        dateEndHour.set(Calendar.SECOND, LAST_SECOND_OF_DAY);
        dateEndHour.set(Calendar.MILLISECOND, LAST_MILISECOND_OF_DAY);
        return dateEndHour.getTime();
    }
    
    /**
     * Method responsible of formtat millisecons in minutes and seconds
     * @param milliseconds millisecons to format
     * @return formtat millisecons in minutes and seconds.
     */
    public static String formatterMinutesSecons(long milliseconds) {
        Long diffSeconds = milliseconds / 1000 % 60;  
        Long diffMinutes = milliseconds / (60 * 1000) % 60;
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MINUTE, diffMinutes.intValue());
        calendar.set(Calendar.SECOND, diffSeconds.intValue());
        String formatHour = new SimpleDateFormat("mm:ss").format(calendar.getTime());
        return formatHour;
    }
    
}
