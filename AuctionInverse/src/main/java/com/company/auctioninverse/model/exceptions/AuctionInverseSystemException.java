package com.company.auctioninverse.model.exceptions;

import com.company.common.model.exceptions.SystemException;

/**
 * Class responsible of handle system exceptions.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
public class AuctionInverseSystemException extends SystemException {
    
    /**
     * Constructor.
     * @param cause cause the error. 
     */
    public AuctionInverseSystemException(Throwable cause) {
        super(cause);
    }
    
    /**
     * Constructor.
     * @param message error message.
     * @param cause cause the error. 
     */
    public AuctionInverseSystemException(String message, Throwable cause) {
        super(message, cause);
    }
    
    /**
     * Constructor.
     * @param message error message.
     */
    public AuctionInverseSystemException(String message) {
        super(message);
    }
    
}
