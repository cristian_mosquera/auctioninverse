

package com.company.auctioninverse.model.service;

import com.company.auctioninverse.model.businessobject.Product;

/**
 * Service that define contract for operations on product entity.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
public interface ProductService extends GenericService<Product>{

}
