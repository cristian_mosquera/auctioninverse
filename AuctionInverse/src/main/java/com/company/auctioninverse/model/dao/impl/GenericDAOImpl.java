package com.company.auctioninverse.model.dao.impl;

import com.company.auctioninverse.model.dao.GenericDAO;
import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;

/**
 * Class that implement contract for the general operations on SGBD.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 * @param <K>  Entity relacionared.
 */
public class GenericDAOImpl <K> 
    extends com.company.common.model.generic.dao.impl.GenericDAOImpl
                                  <K, AuctionInverseSystemException> implements GenericDAO<K> {

}
