package com.company.auctioninverse.model.dao.impl;

import com.company.auctioninverse.model.businessobject.Admin;
import com.company.auctioninverse.model.businessobject.Aspirant;
import com.company.auctioninverse.model.businessobject.Auction;
import com.company.auctioninverse.model.businessobject.AuctionProduct;
import com.company.auctioninverse.model.businessobject.Inscribed;
import com.company.auctioninverse.model.dao.AuctionDAO;
import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

/**
 * Class that implement contract for the auction operations on SGBD.
 *
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
@Repository
public class AuctionDAOImpl extends GenericDAOImpl<Auction> implements AuctionDAO {

    @Override
    public List<Auction> findAuctionsByAdminAndStatus(Admin admin, String status)
            throws AuctionInverseSystemException {
        String sql = "select o from " + Auction.class.getSimpleName() + " o where "
                + "convenning.admin.id = ? and o.status = ?";
        Object[] parameters = {admin.getId(), status};
        return getHibernateTemplate().find(sql, parameters);
    }

    @Override
    public List<Auction> findAuctionsByAdmin(Admin admin, String status,
            Date startDate, Date endDate, String name, Aspirant aspirant)
            throws AuctionInverseSystemException {

        List parameters = new ArrayList();
        String sql = "select o from " + Auction.class.getSimpleName() + " o ";
        sql += " left join fetch o.aspirants aspirants";
        

        if (admin != null && admin.getId() != null) {
            parameters.add(admin.getId());
            sql += " where o.convenning.admin.id = ?";

        }

        if (StringUtils.isNotBlank(status)) {
            parameters.add(status);
            if (sql.contains("where")) {
                sql += " and o.status = ?";
            } else {
                sql += " where o.status = ?";
            }
        }

        if (startDate != null) {
            parameters.add(startDate);
            if (sql.contains("where")) {
                sql += " and o.createdDate >= ?";
            } else {
                sql += " where o.createdDate >= ?";
            }
        }

        if (endDate != null) {
            parameters.add(endDate);
            if (sql.contains("where")) {
                sql += " and o.createdDate < ?";
            } else {
                sql += " where o.createdDate < ?";
            }
        }

        if (StringUtils.isNotBlank(name)) {
            parameters.add("%" + name.toUpperCase() + "%");
            if (sql.contains("where")) {
                sql += " and UPPER(o.name) like ?";
            } else {
                sql += " where UPPER(o.name) like ?";
            }
        }
        if (aspirant != null && StringUtils.isNotBlank(aspirant.getName())) {
            parameters.add("%" + aspirant.getName().toUpperCase() + "%");
            if (sql.contains("where")) {
                sql += " and UPPER(CONCAT(aspirants.name, ' ', aspirants.lastName)) like ?";
            } else {
                sql += " where UPPER(CONCAT(aspirants.name, ' ', aspirants.lastName)) like ?";
            }
        }

        return getHibernateTemplate().find(sql, parameters.toArray());


    }

    @Override
    public List<Auction> findAuctionsByInscribed(Inscribed inscribed, String status,
            Date startDate, Date endDate, String name)
            throws AuctionInverseSystemException {

        List parameters = new ArrayList();
        String sql = "select o from " + Auction.class.getSimpleName() + " o ";
        if (inscribed != null && inscribed.getId() != null) {
            parameters.add(inscribed.getId());
            sql += " join fetch o.aspirants aspirants  where aspirants.inscribed.id = ?";

        }

        if (StringUtils.isNotBlank(status)) {
            parameters.add(status);
            if (sql.contains("where")) {
                sql += " and o.status = ?";
            } else {
                sql += " where o.status = ?";
            }
        }

        if (startDate != null) {
            parameters.add(startDate);
            if (sql.contains("where")) {
                sql += " and o.createdDate >= ?";
            } else {
                sql += " where o.createdDate >= ?";
            }
        }

        if (endDate != null) {
            parameters.add(endDate);
            if (sql.contains("where")) {
                sql += " and o.createdDate < ?";
            } else {
                sql += " where o.createdDate < ?";
            }
        }

        if (StringUtils.isNotBlank(name)) {
            parameters.add("%" + name.toUpperCase() + "%");
            if (sql.contains("where")) {
                sql += " and upper(o.name) like ?";
            } else {
                sql += " where upper(o.name) like ?";
            }
        }



        return getHibernateTemplate().find(sql, parameters.toArray());
    }

    @Override
    public List<AuctionProduct> findAuctionProductByAuction(Auction auction)
                                                    throws AuctionInverseSystemException {
        List parameters = new ArrayList();
        String sql = "select o from " + AuctionProduct.class.getSimpleName() + " o ";
        if (auction != null && auction.getId() != null) {
           parameters.add(auction.getId());
           sql += " where o.auction.id = ?";
        }
        return getHibernateTemplate().find(sql, parameters.toArray());
    }

    @Override
    public List<Inscribed> findWinnersByAuction(Auction auction) 
                                                    throws AuctionInverseSystemException {
        List parameters = new ArrayList();
        String sql = "select o.winners from " + Auction.class.getSimpleName() + " o ";
        sql += " join o.winners winners";
        if (auction != null && auction.getId() != null) {
            parameters.add(auction.getId());
            sql += " where o.id = ?";
        }
        return getHibernateTemplate().find(sql, parameters.toArray());
    }
    
    
}
