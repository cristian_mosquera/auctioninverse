package com.company.auctioninverse.model.businessobject;

import com.company.common.model.businessobject.IObjectValue;
import com.company.common.model.businessobject.ObjectValue;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

/**
 * Class responsible of represent entity offer.
 *
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
@Entity
public class Offer extends ObjectValue implements Serializable, IObjectValue {
    
    
    @Id
    @GeneratedValue
    private Long id;
    @Cascade(CascadeType.SAVE_UPDATE)
    @ManyToOne
    @JoinColumn(name = "inscribed_id", nullable = false)
    @NotNull(message="Inscrito es requerido")
    private Inscribed inscribed;
    @Cascade(CascadeType.SAVE_UPDATE)
    @ManyToOne
    @JoinColumn(name = "round_id", nullable = false)
    private Round round;
   
    @Column(name = "value", nullable = false, precision = 2, scale = 2)
    @Digits(integer = 2,fraction=2, message = 
            "el valor debe ser conformado por uno o varios digitos")
    @NotNull(message="El valor es requerido")
    private Double value;
    @Column(name = "created_date", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @NotNull(message="Fecha de creacion es requerida")
    private Date createdDate;
    @Column(name = "updated_date", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @NotNull(message="Fecha de actualizacion es requerida")
    private Date updatedDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Inscribed getInscribed() {
        return inscribed;
    }

    public void setInscribed(Inscribed inscribed) {
        this.inscribed = inscribed;
    }

    public Round getRound() {
        return round;
    }

    public void setRound(Round round) {
        this.round = round;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 43 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 43 * hash + (this.inscribed != null ? this.inscribed.hashCode() : 0);
        hash = 43 * hash + (this.round != null ? this.round.hashCode() : 0);
        hash = 43 * hash + (this.value != null ? this.value.hashCode() : 0);
        hash = 43 * hash + (this.createdDate != null ? this.createdDate.hashCode() : 0);
        hash = 43 * hash + (this.updatedDate != null ? this.updatedDate.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Offer other = (Offer) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        if (this.inscribed != other.inscribed && (this.inscribed == null 
                || !this.inscribed.equals(other.inscribed))) {
            return false;
        }
        if (this.round != other.round && (this.round == null || !this.round.equals(other.round))) {
            return false;
        }
        if (this.value != other.value && (this.value == null || !this.value.equals(other.value))) {
            return false;
        }
        if (this.createdDate != other.createdDate && (this.createdDate == null 
                || !this.createdDate.equals(other.createdDate))) {
            return false;
        }
        if (this.updatedDate != other.updatedDate && (this.updatedDate == null 
                || !this.updatedDate.equals(other.updatedDate))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Offer{" + "id=" + id + ", inscribed=" + inscribed + ", round=" + round + ", "
                + "value=" + value + ", createdDate=" + createdDate 
                + ", updatedDate=" + updatedDate + '}';
    }
    
   
}
