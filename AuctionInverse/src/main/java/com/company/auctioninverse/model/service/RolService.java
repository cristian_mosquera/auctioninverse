

package com.company.auctioninverse.model.service;

import com.company.auctioninverse.model.businessobject.Rol;

/**
 * Service that define contract for operations on rol entity.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
public interface RolService extends GenericService<Rol>{

}
