package com.company.auctioninverse.model.businessobject;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

/**
 * Class responsible of represent entity aspirant.
 *
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
@Entity
@DiscriminatorValue("ADMIN")
public class Aspirant extends Person {
    
    
    @JoinColumn(nullable = false, name = "auction_id")
    @Cascade(CascadeType.SAVE_UPDATE)
    @ManyToOne()
    private Auction auction;
    @Cascade({CascadeType.SAVE_UPDATE, CascadeType.DELETE})
    @OneToOne(mappedBy = "aspirant")
    private Inscribed inscribed;
    
    
   

    public Auction getAuction() {
        return auction;
    }

    public void setAuction(Auction auction) {
        this.auction = auction;
    }

    public Inscribed getInscribed() {
        return inscribed;
    }

    public void setInscribed(Inscribed inscribed) {
        this.inscribed = inscribed;
    }
    
    

    @Override
    public String toString() {
        return super.toString() + "Aspirant{" 
                    + ", auction=" + auction + '}';
    }
    
    
    
}
