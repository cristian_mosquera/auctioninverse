package com.company.auctioninverse.model.service.impl;

import com.company.auctioninverse.model.businessobject.Admin;
import com.company.auctioninverse.model.dao.AdminDAO;
import com.company.auctioninverse.model.service.AdminService;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service that implement contract for operations on admin entity.
 *
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
@Service(value = "adminService")
public class AdminServiceImpl extends GenericServiceImpl<Admin> implements AdminService {
    
    @Autowired
    private AdminDAO adminDAO;
    
    
    /**
     * Method responsible of init class.
     */
    @PostConstruct
    public void init() {
        adminDAO.setSessionFactory(sessionFactory);
        setGenericDao(adminDAO);
    }    
}
