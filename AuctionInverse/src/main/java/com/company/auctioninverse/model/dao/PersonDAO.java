

package com.company.auctioninverse.model.dao;

import com.company.auctioninverse.model.businessobject.Person;
import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import org.hibernate.SessionFactory;

/**
 * Service that define contract for operations on person entity.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
public interface PersonDAO {
    
    /**
     * Method responsible of find person by username.
     * @param user user to finding.
     * @return person by username.
     * @throws AuctionInverseSystemException any system error.
     */
    Person findByUserName (String user) throws AuctionInverseSystemException;
    
    /**
     * Change session factory.
     * @param sessionFactory  session factory.
     * 
     */
    void setSessionFactory(SessionFactory sessionFactory);
}
