package com.company.auctioninverse.model.businessobject;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Class responsible of handle entity inscribed product.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Mosquera</a>
 */
@DiscriminatorValue("INSCRIBEDPRODUCT")
@Entity
public class InscribedProduct extends ProductRelation {
    
    private Inscribed inscribed;
    
    @ManyToOne
    @JoinColumn(name = "inscribed_id", nullable = false)
    public Inscribed getInscribed() {
        return inscribed;
    }

    public void setInscribed(Inscribed inscribed) {
        this.inscribed = inscribed;
    }
    
    @Override
    public String toString() {
        return super.toString() + "InscribedProduct{" + "inscribed=" + inscribed + '}';
    }
}
