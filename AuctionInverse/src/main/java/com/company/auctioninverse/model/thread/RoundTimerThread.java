package com.company.auctioninverse.model.thread;

import com.company.auctioninverse.model.businessobject.Round;
import java.util.Timer;
import org.apache.log4j.Logger;

/**
 * Thread responsible of handle time of round
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Mosquera</a>
 */
public class RoundTimerThread implements Runnable {
    
    
    private Round round;
    private Logger logger = Logger.getLogger(getClass());

    public RoundTimerThread(Round round) {
        this.round = round;
    }

    @Override
    public void run() {
        final Timer timer = new Timer();
        timer.schedule(new RoundTimerTask(round, timer, null), 0, 1234);
    }
    
    
    
}
