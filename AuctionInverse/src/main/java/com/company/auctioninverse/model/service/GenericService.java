
package com.company.auctioninverse.model.service;

import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import com.company.auctioninverse.model.exceptions.AuctionInverseValidationException;
import java.io.Serializable;

/**
 * Service that define contract for the general operations  on SGBD.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 * @param <K> Entity relacionared.
 */
public interface GenericService <K> extends com.company.common.model.generic.service.GenericService
            <K, AuctionInverseSystemException, AuctionInverseValidationException>, Serializable  {
    
}
