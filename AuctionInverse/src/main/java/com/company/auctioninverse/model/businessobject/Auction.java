package com.company.auctioninverse.model.businessobject;

import com.company.common.model.businessobject.IObjectValue;
import com.company.common.model.businessobject.ObjectValue;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

/**
 * Class responsible of represent entity auction.
 *
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
@Entity
public class Auction extends ObjectValue implements Serializable, IObjectValue {
    
    
    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false, length = 255, name = "description")
    private String description;
    @Column(nullable = false, length = 100, name = "name")
    private String name;
    @Column(nullable = false, length = 15, name = "status")
    private String status; 
    @Column(nullable = false, precision = 10, scale = 2, name = "startValue")
    private Double startValue;
    @ManyToOne
    @Cascade(CascadeType.SAVE_UPDATE)
    @JoinColumn(nullable = false, name = "convenning_id")
    private Convenning convenning;
    @Cascade({CascadeType.SAVE_UPDATE, CascadeType.DELETE})
    @OneToMany(mappedBy = "auction")
    private List<Round> rounds;
    @JoinTable(name="auction_inscribed", 
      joinColumns=@JoinColumn(name="auction_id"),
      inverseJoinColumns=@JoinColumn(name="inscribed_id"), 
      uniqueConstraints=@UniqueConstraint(columnNames={"auction_id", "inscribed_id"}))
    @Cascade({CascadeType.SAVE_UPDATE, CascadeType.DELETE})
    @OneToMany()
    private List<Inscribed> winners;
    @Column(name = "created_date", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "updated_date", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @Cascade({CascadeType.SAVE_UPDATE, CascadeType.DELETE})
    @OneToMany(mappedBy = "auction")
    private List<Aspirant> aspirants;
    @Column(name = "time_for_round", length = 15)
    private Long timeForRound;
    @Column(name = "min_porcent", precision = 2, scale = 2)
    private Double minPorcent;
    @OneToMany(mappedBy = "auction")
    @Cascade({CascadeType.SAVE_UPDATE})
    private List<AuctionProduct> auctionProducts;
    @Column(name = "start_date", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

  
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Convenning getConvenning() {
        return convenning;
    }

    public void setConvenning(Convenning convenning) {
        this.convenning = convenning;
    }

    public List<Round> getRounds() {
        return rounds;
    }

    public void setRounds(List<Round> rounds) {
        this.rounds = rounds;
    }

    public List<Inscribed> getWinners() {
        return winners;
    }

    public void setWinners(List<Inscribed> winners) {
        this.winners = winners;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Double getStartValue() {
        return startValue;
    }

    public void setStartValue(Double startValue) {
        this.startValue = startValue;
    }

    public List<Aspirant> getAspirants() {
        return aspirants;
    }

    public void setAspirants(List<Aspirant> aspirants) {
        this.aspirants = aspirants;
    }

    public Long getTimeForRound() {
        return timeForRound;
    }

    public void setTimeForRound(Long timeForRound) {
        this.timeForRound = timeForRound;
    }

    public Double getMinPorcent() {
        return minPorcent;
    }

    public void setMinPorcent(Double minPorcent) {
        this.minPorcent = minPorcent;
    }

    public List<AuctionProduct> getAuctionProducts() {
        return auctionProducts;
    }

    public void setAuctionProducts(List<AuctionProduct> auctionProducts) {
        this.auctionProducts = auctionProducts;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    
    
    
    
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 71 * hash + (this.description != null ? this.description.hashCode() : 0);
        hash = 71 * hash + (this.convenning != null ? this.convenning.hashCode() : 0);
        hash = 71 * hash + (this.createdDate != null ? this.createdDate.hashCode() : 0);
        hash = 71 * hash + (this.updatedDate != null ? this.updatedDate.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Auction other = (Auction) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        if ((this.description == null) ? (other.description != null) : 
                !this.description.equals(other.description)) {
            return false;
        }
        if (this.convenning != other.convenning && (this.convenning == null || 
                !this.convenning.equals(other.convenning))) {
            return false;
        }
        
        if (this.createdDate != other.createdDate && (this.createdDate == null 
                || !this.createdDate.equals(other.createdDate))) {
            return false;
        }
        if (this.updatedDate != other.updatedDate && (this.updatedDate == null 
                || !this.updatedDate.equals(other.updatedDate))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Auction{" + "id=" + id + ", description=" + description 
                + ", startValue=" + startValue + ", convenning=" + convenning 
                + ", createdDate=" + createdDate + ", updatedDate=" 
                + updatedDate + ",name" + name + 
                ",status=" + status +  ",startDate=" + startDate +'}';
    }

    
        
    
    
}
