package com.company.auctioninverse.model.businessobject;

import com.company.common.model.businessobject.IObjectValue;
import com.company.common.model.businessobject.ObjectValue;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

/**
 * Class responsible of represent entity person.
 *
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name="PersonDiscriminator", discriminatorType= DiscriminatorType.STRING)
public class Person extends ObjectValue implements Serializable, IObjectValue {
    
    @Id
    @GeneratedValue
    private Long id;
    @Column(name = "name", length = 60, nullable = false)
    private String name;
    @Column(name = "last_name", length = 60, nullable = false)
    private String lastName;
    @Column(name = "gener", length = 1, nullable = false)
    private String gener;
    @Column(name = "birth_date", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date birthDate;
    @Column(name = "identification_number", length = 15, nullable = false)
    private String identificationNumber;
    @Column(name = "identification_type", length = 15, nullable = false)
    private String identificationType;
    @Column(name = "phone", length = 15, nullable = false)
    private String phone;
    @Column(name = "email", length = 150, nullable = false)
    private String email;
    @Column(name = "user_name", length = 150)
    private String userName;
    @Column(name = "password", length = 150)
    private String password;
    @ManyToOne
    @JoinColumn(name = "rol_id", nullable = false)
    @Cascade(CascadeType.SAVE_UPDATE)
    private Rol rol;
    @Column(name = "created_date", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "updated_date", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @Column(nullable = false, length = 15, name = "status")
    private String status; 

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGener() {
        return gener;
    }

    public void setGener(String gener) {
        this.gener = gener;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public String getIdentificationType() {
        return identificationType;
    }

    public void setIdentificationType(String identificationType) {
        this.identificationType = identificationType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 17 * hash + (this.name != null ? this.name.hashCode() : 0);
        hash = 17 * hash + (this.lastName != null ? this.lastName.hashCode() : 0);
        hash = 17 * hash + (this.gener != null ? this.gener.hashCode() : 0);
        hash = 17 * hash + (this.birthDate != null ? this.birthDate.hashCode() : 0);
        hash = 17 * hash + (this.identificationNumber != null ? 
                this.identificationNumber.hashCode() : 0);
        hash = 17 * hash + (this.identificationType != null ? 
                this.identificationType.hashCode() : 0);
        hash = 17 * hash + (this.email != null ? this.email.hashCode() : 0);
        hash = 17 * hash + (this.userName != null ? this.userName.hashCode() : 0);
        hash = 17 * hash + (this.password != null ? this.password.hashCode() : 0);
        hash = 17 * hash + (this.rol != null ? this.rol.hashCode() : 0);
        hash = 17 * hash + (this.createdDate != null ? this.createdDate.hashCode() : 0);
        hash = 17 * hash + (this.updatedDate != null ? this.updatedDate.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Person other = (Person) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        if ((this.lastName == null) ? (other.lastName != null) : 
                !this.lastName.equals(other.lastName)) {
            return false;
        }
        if ((this.gener == null) ? (other.gener != null) : !this.gener.equals(other.gener)) {
            return false;
        }
        
        if ((this.identificationNumber == null) ? (other.identificationNumber != null) : 
                !this.identificationNumber.equals(other.identificationNumber)) {
            return false;
        }
        if ((this.identificationType == null) ? (other.identificationType != null) : 
                !this.identificationType.equals(other.identificationType)) {
            return false;
        }
        if ((this.email == null) ? (other.email != null) : !this.email.equals(other.email)) {
            return false;
        }
        if ((this.userName == null) ? (other.userName != null) :
                !this.userName.equals(other.userName)) {
            return false;
        }
        if ((this.password == null) ? (other.password != null) : 
                !this.password.equals(other.password)) {
            return false;
        }
        if (this.rol != other.rol && (this.rol == null || !this.rol.equals(other.rol))) {
            return false;
        }
        if (this.createdDate != other.createdDate && (this.createdDate == null || 
                !this.createdDate.equals(other.createdDate))) {
            return false;
        }
        if (this.updatedDate != other.updatedDate && (this.updatedDate == null || 
                !this.updatedDate.equals(other.updatedDate))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Person{" + "id=" + id + ", name=" + name + ", lastName=" + lastName + ", gener=" +
                gener + ", birthDate=" + birthDate + ", identificationNumber=" 
                + identificationNumber + ", identificationType=" + identificationType 
                + ", email=" + email + ", userName=" + userName + ", password=" + password 
                + ", rol=" + rol + ", createdDate=" + createdDate + ", phone=" + phone 
                + ", updatedDate=" + updatedDate + '}';
    }

    
    
   
}
