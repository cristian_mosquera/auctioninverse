package com.company.auctioninverse.model.exceptions;

import com.company.common.model.exceptions.SystemRunTimeException;

/**
 * Class responsible of handle system runtime exceptions.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
public class AuctionInverseSystemRunTimeException extends SystemRunTimeException {

    /**
     * Constructor.
     *
     * @param cause cause the error.
     */
    public AuctionInverseSystemRunTimeException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructor.
     *
     * @param message error message.
     * @param cause cause the error.
     */
    public AuctionInverseSystemRunTimeException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructor.
     *
     * @param message error message.
     */
    public AuctionInverseSystemRunTimeException(String message) {
        super(message);
    }

}
