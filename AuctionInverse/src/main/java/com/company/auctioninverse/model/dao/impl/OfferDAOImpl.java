package com.company.auctioninverse.model.dao.impl;

import com.company.auctioninverse.model.businessobject.Offer;
import com.company.auctioninverse.model.dao.OfferDAO;
import org.springframework.stereotype.Repository;

/**
 * Class that implement contract for the offer operations  on SGBD. 
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
@Repository
public class OfferDAOImpl extends GenericDAOImpl<Offer> implements OfferDAO {
    
}
