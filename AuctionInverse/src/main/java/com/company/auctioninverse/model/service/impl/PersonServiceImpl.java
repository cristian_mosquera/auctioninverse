package com.company.auctioninverse.model.service.impl;

import com.company.auctioninverse.model.businessobject.Person;
import com.company.auctioninverse.model.dao.PersonDAO;
import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import com.company.auctioninverse.model.exceptions.AuctionInverseValidationException;
import com.company.auctioninverse.model.service.PersonService;
import javax.annotation.PostConstruct;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service that implement contract for operations on admin entity.
 *
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
@Service(value = "personService")
public class PersonServiceImpl implements PersonService {
    
    @Autowired
    private PersonDAO personDAO;
    @Autowired
    protected SessionFactory sessionFactory;
    
    /**
     * Method responsible of init class.
     */
    @PostConstruct
    public void init() {
        personDAO.setSessionFactory(sessionFactory);
    }    

    @Override
    public Person login(String user, String password) 
            throws AuctionInverseSystemException, AuctionInverseValidationException {
         
        Person personFind = personDAO.findByUserName(user);
        if (personFind == null) {
            throw  new AuctionInverseValidationException("El usuario " + user + " no existe");
        }
        if(!password.trim().equals(personFind.getPassword().trim())) {
            throw  new AuctionInverseValidationException("Verifique su clave");
        }    
        return personFind;
    }
}
