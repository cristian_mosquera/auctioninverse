package com.company.auctioninverse.model.thread;

import com.company.auctioninverse.model.businessobject.Round;
import com.company.auctioninverse.model.dao.MongoDAO;
import com.company.auctioninverse.model.dao.impl.MongoDAOImpl;
import com.company.auctioninverse.model.service.AuctionService;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import org.apache.log4j.Logger;

/**
 * Thread responsible of handle time of round
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Mosquera</a>
 */
public class RoundTimerTask extends TimerTask {
    
    private Date initialExcecute = new Date();
    private Round round;
    private Timer timer;
    private Logger logger = Logger.getLogger(getClass());
    private AuctionService auctionService;

    public RoundTimerTask(Round round, Timer timer, AuctionService auctionService) {
        this.round = round;
        this.timer = timer;
        this.auctionService = auctionService;
    }

    @Override
    public void run() {
        long milliseconsTranscurred = new Date().getTime() - initialExcecute.getTime();
        try {
            doStuff(milliseconsTranscurred);
        } 
        catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        
        if (milliseconsTranscurred >= round.getAuction().getTimeForRound()) {
            timer.cancel();
            Round newRound = new Round();
            newRound.setAuction(round.getAuction());
            try {
                auctionService.newRound(newRound);
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                e.printStackTrace();
            }
            return;
        }
    }
    
    /**
     * Method responsible of do stuff.
     * @param milliseconds millisecons transcurred.
     * @throws Exception any error.
     */
    public void doStuff(long milliseconds) throws Exception {
        Long diffSeconds = milliseconds / 1000 % 60;  
        Long diffMinutes = milliseconds / (60 * 1000) % 60;
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MINUTE, diffMinutes.intValue());
        calendar.set(Calendar.SECOND, diffSeconds.intValue());
        MongoDAO mongoDAO = new MongoDAOImpl();
        DBObject obj = new BasicDBObject();
        obj.put("id", round.getId());
        DBObject objFind = mongoDAO.loadObject(obj);
        String formatHour = new SimpleDateFormat("mm:ss").format(calendar.getTime());
        if (objFind == null) {
            DBObject objInsert = new BasicDBObject();
            objInsert.put("id", round.getId());
            objInsert.put("timeTranscurred", formatHour);
            mongoDAO.save(objInsert);
        }
        else {
            DBObject objUpdate = new BasicDBObject();
            objUpdate.put("id", round.getId());
            objUpdate.put("timeTranscurred", formatHour);
            mongoDAO.update(objFind, objUpdate);
        }
        
        
    }
    
}
