

package com.company.auctioninverse.model.service;

import com.company.auctioninverse.model.businessobject.Offer;

/**
 * Service that define contract for operations on offer entity.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
public interface OfferService extends GenericService<Offer>{

}
