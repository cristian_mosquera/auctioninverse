package com.company.auctioninverse.model.dao;

import com.company.auctioninverse.model.businessobject.Admin;

/**
 * Class that define contract for the admin operations on SGBD.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
public interface AdminDAO extends GenericDAO<Admin> {
    
    
    
    
}
