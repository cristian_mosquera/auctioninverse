

package com.company.auctioninverse.model.service;

import com.company.auctioninverse.model.businessobject.Person;
import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import com.company.auctioninverse.model.exceptions.AuctionInverseValidationException;
import java.io.Serializable;

/**
 * Service that define contract for operations on person entity.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
public interface PersonService extends Serializable{
    
    /**
     * Method responsible of sing in in the application.
     * @param user user to sing in.
     * @param password password of user.
     * @return person logged.
     * @throws AuctionInverseSystemException any system error.
     * @throws AuctionInverseValidationException any validation error.
     */
    Person login (String user, String password)
            throws AuctionInverseSystemException, AuctionInverseValidationException;
}
