

package com.company.auctioninverse.model.service;

import com.company.auctioninverse.model.businessobject.Auction;

/**
 * Service that define contract for operations on inscribed entity.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
public interface AuctionInscribed extends GenericService<Auction> {

}
