package com.company.auctioninverse.model.businessobject;

import java.util.List;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

/**
 * Class responsible of represent admin entity.
 *
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
@DiscriminatorValue("ADMIN")
@Entity
public class Admin extends Person {
    
    @Cascade({CascadeType.ALL})
    @OneToMany(mappedBy = "admin")
    private List<Convenning> convennings;

    public List<Convenning> getConvennings() {
        return convennings;
    }

    public void setConvennings(List<Convenning> convennings) {
        this.convennings = convennings;
    }
  
    
}
