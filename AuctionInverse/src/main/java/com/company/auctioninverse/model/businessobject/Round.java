package com.company.auctioninverse.model.businessobject;

import com.company.common.model.businessobject.IObjectValue;
import com.company.common.model.businessobject.ObjectValue;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

/**
 * Class responsible of represent entity round.
 *
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
@Entity
public class Round extends ObjectValue implements Serializable, IObjectValue {
    
    @Id
    @GeneratedValue
    private Long id;
    @Cascade(CascadeType.SAVE_UPDATE)
    @ManyToOne
    @JoinColumn(nullable = false, name = "auction_id")
    private Auction auction;
    @Column(nullable = false, name = "number", length = 3)
    private Short number;
    @Cascade({CascadeType.SAVE_UPDATE, CascadeType.DELETE})
    @OneToMany(mappedBy = "round")
    private List<Offer> offers;
    @OneToMany
    @Cascade({CascadeType.SAVE_UPDATE, CascadeType.DELETE})
    @JoinTable(name="inscribeds_round", 
      joinColumns=@JoinColumn(name="round_id"),
      inverseJoinColumns=@JoinColumn(name="inscribed_id"), 
      uniqueConstraints=@UniqueConstraint(columnNames={"round_id", "inscribed_id"}))
    private List<Inscribed> inscribeds;
    @Cascade({CascadeType.SAVE_UPDATE, CascadeType.DELETE})
    @OneToMany()
    @JoinTable(name="winners_round", 
      joinColumns=@JoinColumn(name="round_id"),
      inverseJoinColumns=@JoinColumn(name="inscribed_id"), 
      uniqueConstraints=@UniqueConstraint(columnNames={"round_id", "inscribed_id"}))
    private List<Inscribed> winners;
    @Column(name = "created_date", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "updated_date", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @Column(nullable = false, length = 15, name = "status")
    private String status; 
    @Column(nullable = false, name = "current")
    private Boolean current;
    @Transient
    private String timeTranscurred;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Auction getAuction() {
        return auction;
    }

    public void setAuction(Auction auction) {
        this.auction = auction;
    }

    public List<Offer> getOffers() {
        return offers;
    }

    public void setOffers(List<Offer> offers) {
        this.offers = offers;
    }

    public List<Inscribed> getInscribeds() {
        return inscribeds;
    }

    public void setInscribeds(List<Inscribed> inscribeds) {
        this.inscribeds = inscribeds;
    }

    public List<Inscribed> getWinners() {
        return winners;
    }

    public void setWinners(List<Inscribed> winners) {
        this.winners = winners;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Short getNumber() {
        return number;
    }

    public void setNumber(Short number) {
        this.number = number;
    }

    public Boolean isCurrent() {
        return current;
    }

    public void setCurrent(Boolean current) {
        this.current = current;
    }

    public String getTimeTranscurred() {
        return timeTranscurred;
    }

    public void setTimeTranscurred(String timeTranscurred) {
        this.timeTranscurred = timeTranscurred;
    }
    
    
    
    
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 71 * hash + (this.auction != null ? this.auction.hashCode() : 0);
        hash = 71 * hash + (this.createdDate != null ? this.createdDate.hashCode() : 0);
        hash = 71 * hash + (this.updatedDate != null ? this.updatedDate.hashCode() : 0);
        
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Round other = (Round) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        if (this.auction != other.auction && (this.auction == null || 
                !this.auction.equals(other.auction))) {
            return false;
        }
        return true;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    

    @Override
    public String toString() {
        return "Round{" + "id=" + id + ", auction=" + auction + ", offers=" 
                + offers + ", inscribeds=" + inscribeds + ", winners=" 
                + winners + ", createdDate=" + createdDate + ", updatedDate=" + updatedDate + '}';
    }

    
    
    
    
    
    
}
