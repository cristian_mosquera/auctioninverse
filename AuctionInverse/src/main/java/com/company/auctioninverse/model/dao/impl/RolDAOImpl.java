package com.company.auctioninverse.model.dao.impl;

import com.company.auctioninverse.model.businessobject.Rol;
import com.company.auctioninverse.model.dao.RolDAO;
import org.springframework.stereotype.Repository;

/**
 * Class that implement contract for the rol operations  on SGBD. 
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
@Repository
public class RolDAOImpl extends GenericDAOImpl<Rol> implements RolDAO {
    
}
