

package com.company.auctioninverse.model.service;

import com.company.auctioninverse.model.businessobject.Convenning;

/**
 * Service that define contract for operations on convenning entity.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
public interface ConvenningService extends GenericService<Convenning>{

}
