package com.company.auctioninverse.model.dao;

import com.company.auctioninverse.model.businessobject.AuctionProduct;

/**
 * Class that define contract for the auction product operations on SGBD.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
public interface AuctionProductDAO extends GenericDAO<AuctionProduct> {
    
    
    
    
}
