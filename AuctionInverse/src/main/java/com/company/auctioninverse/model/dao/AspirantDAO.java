package com.company.auctioninverse.model.dao;

import com.company.auctioninverse.model.businessobject.Aspirant;
import com.company.auctioninverse.model.businessobject.Auction;
import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import java.util.List;

/**
 * Class that define contract for the aaspirant operations on SGBD.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
public interface AspirantDAO extends GenericDAO<Aspirant> {
    
    /**
     * Method responsible of load aspirant by auction.
     * @param auction auction for filter
     * @return list of aspirants by auction.
     * @throws AuctionInverseSystemException any system error.
     */
    List<Aspirant> findAspirantsByAuction(Auction auction) throws AuctionInverseSystemException;
    
    
}
