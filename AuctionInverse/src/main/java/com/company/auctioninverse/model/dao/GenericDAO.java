
package com.company.auctioninverse.model.dao;

import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import java.io.Serializable;

/**
 * Class that define contract for the general operations  on SGBD.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 * @param <K> Entity relacionared.
 */
public interface GenericDAO <K> 
    extends com.company.common.model.generic.dao.GenericDAO <K, AuctionInverseSystemException>, Serializable  {
     
   
}
