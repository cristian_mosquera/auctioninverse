package com.company.auctioninverse.model.dao;

import com.company.auctioninverse.model.businessobject.Offer;

/**
 * Class that define contract for the offer operations on SGBD.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
public interface OfferDAO extends GenericDAO<Offer> {
    
}
