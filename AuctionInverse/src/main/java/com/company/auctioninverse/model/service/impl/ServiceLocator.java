package com.company.auctioninverse.model.service.impl;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * Clase responsible of locator services.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Mosquera</a>
 */
public class ServiceLocator {
    
    public static Object getService(String serviceName) {
        ServletContext context =(ServletContext) FacesContext.
                getCurrentInstance().getExternalContext().getContext();
        ApplicationContext applicationContext = WebApplicationContextUtils.
               getRequiredWebApplicationContext(context);
       return applicationContext.getBean(serviceName);
    }
}
