package com.company.auctioninverse.model.dao.impl;

import com.company.auctioninverse.model.businessobject.Aspirant;
import com.company.auctioninverse.model.businessobject.Auction;
import com.company.auctioninverse.model.dao.AspirantDAO;
import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 * Class that implement contract for the aspirant operations  on SGBD. 
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
@Repository
public class AspirantDAOImpl extends GenericDAOImpl<Aspirant> implements AspirantDAO {

    @Override
    public List<Aspirant> findAspirantsByAuction(Auction auction) throws AuctionInverseSystemException {
        String sql = "from " + Aspirant.class.getSimpleName() + " where auction.id = ? ";
        return getHibernateTemplate().find(sql, auction.getId());
    }
    
}
