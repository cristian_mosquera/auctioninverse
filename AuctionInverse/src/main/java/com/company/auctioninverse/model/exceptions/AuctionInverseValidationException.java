package com.company.auctioninverse.model.exceptions;

import com.company.common.model.exceptions.ValidationException;

/**
 * Class responsible of handle validation exceptions.
 *
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
public class AuctionInverseValidationException extends ValidationException {

    /**
     * Constructor.
     *
     * @param cause cause the error.
     */
    public AuctionInverseValidationException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructor.
     *
     * @param message error message.
     * @param cause cause the error.
     */
    public AuctionInverseValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructor.
     *
     * @param message error message.
     */
    public AuctionInverseValidationException(String message) {
        super(message);
    }

}
