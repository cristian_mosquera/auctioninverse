package com.company.auctioninverse.model.service.impl;

import com.company.auctioninverse.model.businessobject.Rol;
import com.company.auctioninverse.model.dao.RolDAO;
import com.company.auctioninverse.model.service.RolService;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service that implement contract for operations on rol entity.
 *
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
@Service(value = "rolService")
public class RolServiceImpl extends GenericServiceImpl<Rol> implements RolService {
    
    @Autowired
    private RolDAO rolDAO;
    
    
    /**
     * Method responsible of init class.
     */
    @PostConstruct
    public void init() {
        rolDAO.setSessionFactory(sessionFactory);
        setGenericDao(rolDAO);
    }
}
