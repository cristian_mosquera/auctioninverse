package com.company.auctioninverse.model.dao.impl;

import com.company.auctioninverse.model.businessobject.Person;
import com.company.auctioninverse.model.dao.PersonDAO;
import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

/**
 * Class that implement contract for the person operations  on SGBD. 
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
@Repository
public class PersonDAOImpl implements PersonDAO {
    
    private HibernateTemplate hibernateTemplate;
    
    @Override
    public Person findByUserName(String user) throws AuctionInverseSystemException {
        List parameters = new ArrayList();
        String sql = "select o from " + Person.class.getSimpleName() + " o ";
        
        if (StringUtils.isNotBlank(user)) {
            parameters.add(user);
            sql += " where o.userName = ?";
        }
        Person person = null;
        Iterator<Person> iterator = getHibernateTemplate().
                    find(sql, parameters.toArray()).iterator();
        if (iterator.hasNext()){
            person = iterator.next();
        } 
        
        return person;
    }
    
    
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        hibernateTemplate = new HibernateTemplate(sessionFactory);
        
    }

    public HibernateTemplate getHibernateTemplate() {
        return hibernateTemplate;
    }

    public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
        this.hibernateTemplate = hibernateTemplate;
    }
    
    
}
