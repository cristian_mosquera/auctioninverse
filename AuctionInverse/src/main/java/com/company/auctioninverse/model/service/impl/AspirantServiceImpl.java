package com.company.auctioninverse.model.service.impl;

import com.company.auctioninverse.model.businessobject.Aspirant;
import com.company.auctioninverse.model.businessobject.Auction;
import com.company.auctioninverse.model.dao.AspirantDAO;
import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import com.company.auctioninverse.model.exceptions.AuctionInverseValidationException;
import com.company.auctioninverse.model.service.AspirantService;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service that implement contract for operations on aspirant entity.
 *
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
@Service(value = "aspirantService")
public class AspirantServiceImpl extends GenericServiceImpl<Aspirant> implements AspirantService {
    
    @Autowired
    private AspirantDAO aspirantDAO;
    
    /**
     * Method responsible of init class.
     */
    @PostConstruct
    public void init() {
        aspirantDAO.setSessionFactory(sessionFactory);
        setGenericDao(aspirantDAO);
    }
    
    @Override
    public List<Aspirant> findAspirantsByAuction(Auction auction) 
            throws AuctionInverseSystemException, AuctionInverseValidationException {
        try {
            return aspirantDAO.findAspirantsByAuction(auction);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new AuctionInverseSystemException(e.getMessage(), e);
        }
        
    }
    
    @Override
    public void loadAspirantsByAuctions(List<Auction> auctions) 
            throws AuctionInverseSystemException, AuctionInverseValidationException {
        try {
            for (Auction auction : auctions) {
            List<Aspirant> aspirants = findAspirantsByAuction(auction);
            auction.setAspirants(aspirants);
        }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new AuctionInverseSystemException(e.getMessage(), e);
        }
    }
}
