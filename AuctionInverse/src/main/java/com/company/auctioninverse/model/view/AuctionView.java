package com.company.auctioninverse.model.view;

import com.company.auctioninverse.model.businessobject.Admin;
import com.company.auctioninverse.model.businessobject.Aspirant;
import com.company.auctioninverse.model.businessobject.Auction;
import com.company.auctioninverse.model.businessobject.AuctionProduct;
import com.company.auctioninverse.model.businessobject.Inscribed;
import com.company.auctioninverse.model.businessobject.Offer;
import com.company.auctioninverse.model.businessobject.Person;
import com.company.auctioninverse.model.businessobject.Round;
import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import com.company.auctioninverse.model.exceptions.AuctionInverseValidationException;
import com.company.auctioninverse.model.service.AdminService;
import com.company.auctioninverse.model.service.AuctionService;
import com.company.auctioninverse.model.service.InscribedService;
import com.company.auctioninverse.model.service.OfferService;
import com.company.auctioninverse.model.service.RoundService;
import com.company.auctioninverse.model.service.impl.ServiceLocator;
import com.company.auctioninverse.model.util.FormatterUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.Digits;

/**
 * Class responsible of handle event user interface for auction.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Mosquera</a>
 */

@ManagedBean
@SessionScoped
public class AuctionView implements Serializable{
    
    private Person personLogged ;
    
    private  AdminService adminService;
    
    private  AuctionService auctionService;
    
    private  InscribedService inscribedService;
    
    private  RoundService roundService;
    
    private  OfferService offerService;
    
    private String auctionName;
    
    private Date startDate;
    
    private Date endDate;
    
    private String status;
    
    private Aspirant aspirantFilter = new Aspirant();
    
    private List<Auction> auctions = new ArrayList<Auction>();
    
    private List<Inscribed> inscribedsByAction;
    
    private List<Inscribed> winnersByAction;
    
    private List<AuctionProduct> productsByAction;
    
    private List<Round> roundsByAuction;
    
    private Auction auctionSelected;
    
    private Inscribed inscribedSelected;
    
    private Round roundSelected;
    
    private List<Inscribed> inscribesSelected = new ArrayList<Inscribed>();
    
    private List<Offer> offersSelected = new ArrayList<Offer>();
    
    private List<Offer> offersByRound = new ArrayList<Offer>();
    
    private List<Offer> offersByRoundInscribed;
    
    List<Inscribed> winnersByRound = new ArrayList<Inscribed>();
    
    private Offer offer = new Offer();
    
    @Digits(integer = 2, fraction=2)
    private Double valueOffer; 
    
    private Double totalProductAuction = 0.0;
    
    private Round currentRound;
    
    private String offerInScreen = "";
    
    private StringBuilder builderOfferInScreen = new StringBuilder();
    
    private String timeDefined;

    
    
    
    public void loadPolling() {
        loadAuctionSelected(auctionSelected);
        loadOfferInScreen();
        loadTotalProductAuction();
        loadWinnersByRound();
        
    }
    
    
    
    public void loadTimeDefined(Auction auctionSelected) {
        timeDefined = FormatterUtil.formatterMinutesSecons(auctionSelected.getTimeForRound());
    }
    
    @PostConstruct
    public void init() {
        loadServices();
        filterAuctions();
        
    }
    
    public void loadServices() {
        try {
            adminService = (AdminService) 
                            ServiceLocator.getService("adminService");
    
        auctionService = (AuctionService) 
                                ServiceLocator.getService("auctionService");
    
        inscribedService = (InscribedService) 
                                ServiceLocator.getService("inscribedService");
    
        roundService = (RoundService) 
                                ServiceLocator.getService("roundService");
    
        offerService = (OfferService) 
                                ServiceLocator.getService("offerService");
    
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    public void filterAuctions() {
        try {
            if (getPersonLogged()==null) {
                return;
            }
                
            if ("Admin".equals(getPersonLogged().getRol().getName())) {
                if (personLogged instanceof Admin) {
                    Admin admin = (Admin) personLogged;
                    auctions = auctionService.findAuctionsByAdmin(admin, 
                    status, getStartDate(), getEndDate(), auctionName, aspirantFilter);
                }
            }
            else if("Inscribed".equals(personLogged.getRol().getName())){
                if (personLogged instanceof Inscribed) {
                    Inscribed inscribed = (Inscribed) personLogged;
                    auctions = auctionService.findAuctionsByInscribed(inscribed, 
                    status, getStartDate(), getEndDate(), auctionName);
                }
            }
            else if("Aspirant".equals(personLogged.getRol().getName())){
               throw new UnsupportedOperationException();
            }
        } 
        catch (AuctionInverseSystemException e) {
            e.printStackTrace();
        }
        catch (AuctionInverseValidationException e) {
            e.printStackTrace();
        }
        
    }
    public void viewOffers() {
        offersByRound = new ArrayList<Offer>();
        loadOfferByRound(roundSelected, offersByRound);
        offersSelected = offersByRound;
    } 
    
    public void viewInscribedRound() {
        loadAuctionSelected(auctionSelected);
        inscribesSelected = roundSelected.getInscribeds();
    }
    
    public void viewAspirantsAuction() {
        loadAuctionSelected(auctionSelected);
    }
    
    public void viewWinnersAuction() {
        loadAuctionSelected(auctionSelected);
    }
    
    public void viewProductsAuction(){
        loadAuctionSelected(auctionSelected);
    }
    
    public void viewProductsInscribed(){
        loadAuctionSelected(auctionSelected);
    }
    
    public void viewWinnersRound(){
        loadWinnersByRound();
        inscribesSelected = winnersByRound;
    }
     
    public void loadOfferInScreen() {
        builderOfferInScreen.setLength(0);
        if (isAmin()) {
            if (auctionSelected != null  && roundsByAuction != null) {
                for (Round round : roundsByAuction) {
                    builderOfferInScreen.append("Round :");
                    builderOfferInScreen.append(round.getNumber());
                    builderOfferInScreen.append("\n");
                    List<Offer> offerByRound = new ArrayList<Offer>();
                    loadOfferByRound(round,  offerByRound);
                    for (Offer offer : offerByRound) {
                        builderOfferInScreen.append(offer.getInscribed().getUserName());
                        builderOfferInScreen.append(": ");
                        builderOfferInScreen.append(offer.getValue());
                        builderOfferInScreen.append("\n");
                    }
                }
            }
        }
        else if (isInsccribed()) {
            if (auctionSelected != null && roundsByAuction != null) {
                for (Round round : roundsByAuction) {
                    builderOfferInScreen.append("Round :");
                    builderOfferInScreen.append(round.getNumber());
                    builderOfferInScreen.append("\n");
                    List<Offer> offerByRound = new ArrayList<Offer>();
                    loadOfferByRoundInscribed(round,(Inscribed) personLogged, offerByRound);
                    
                    for (Offer offer : offerByRound) {
                        builderOfferInScreen.append(offer.getInscribed().getUserName());
                        builderOfferInScreen.append(": ");
                        builderOfferInScreen.append(offer.getValue());
                        builderOfferInScreen.append("\n");
                    }
                    
                }
            }
        }
        offerInScreen = builderOfferInScreen.toString();
        offer.setValue(null);
    }
    public void loadTotalProductAuction() {
        try {
            if (auctionSelected != null) {
                for (AuctionProduct auctionProduct : auctionService.
                            findAuctionProductByAuction(auctionSelected)) {
                    totalProductAuction += auctionProduct.getSubTotal();
                }
            }
        } 
        catch (AuctionInverseSystemException e) {
            e.printStackTrace();
        }
        catch (AuctionInverseValidationException e) {
            e.printStackTrace();
        }
    }
    
    public Double getTotalProductAuction() {
        return totalProductAuction;
    }
    public String selectAuction() {
        loadAuctionSelected(auctionSelected);
        loadOfferInScreen();
        if (isAmin()) {
            return "selectAuctionAdmin";
        } 
        else if (isInsccribed()) {
            return "selectAuctionInscribed";
        } 
        else if ("Aspirant".equals(personLogged.getRol().getName())) {
            return "selectAuctionAspirant";
        }
        return  null;
    }
    
    public void sendOffer() {
        try {
            if (personLogged instanceof Inscribed) {
                offer.setInscribed((Inscribed) personLogged);
            } else {
                throw new AuctionInverseValidationException("Se esperaba un incrito para ofertar");
            }
            offer.setRound(currentRound);
            auctionService.saveOffer(offer);
            loadAuctionSelected(auctionSelected);
            loadOfferInScreen();
        } catch (AuctionInverseSystemException e) {
            e.printStackTrace();
        } catch (AuctionInverseValidationException e) {
            e.printStackTrace();
        }

    }
    
    public void changeStatus() {
        try {
            if (auctionSelected != null) {
                if ("STARTED".equals(auctionSelected.getStatus())) {
                    auctionService.startAuction(auctionSelected);
                }
                else {
                  auctionService.update(auctionSelected);  
                }
                loadAuctionSelected(auctionSelected);
                
            }    
        } 
        catch (AuctionInverseSystemException e) {
            e.printStackTrace();
        }
        catch (AuctionInverseValidationException e) {
            e.printStackTrace();
        }
    }
    public List<Auction> getAuctions() {
        return auctions;
    }

    public void setAuctions(List<Auction> auctions) {
        this.auctions = auctions;
    }

    public AuctionService getAuctionService() {
        return auctionService;
    }

    public void setAuctionService(AuctionService auctionService) {
        this.auctionService = auctionService;
    }

    public Person getPersonLogged() {
        try {
            if (personLogged == null) {
//                personLogged = personView.getPersonLogged();
                HttpSession session = (HttpSession) FacesContext.getCurrentInstance().
                  getExternalContext().getSession(false);
                personLogged = (Person) session.getAttribute("personLogged");
//                personLogged = adminService.findAll(Admin.class).iterator().next();
//                personLogged = inscribedService.findAll(Inscribed.class).iterator().next();
                

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return personLogged  ;
    }

    public void setPersonLogged(Person personLogged) {
        this.personLogged = personLogged;
    }

    
    public String getAuctionName() {
        return auctionName;
    }

    public void setAuctionName(String auctionName) {
        this.auctionName = auctionName;
    }

    public Date getStartDate() {
        if (startDate != null) {
            FormatterUtil.getDateWithStartHourOfDay(startDate);
        }
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        if (endDate != null) {
            FormatterUtil.getDateWithEndHourOfDay(endDate);
        }
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

   
    public AdminService getAdminService() {
        return adminService;
    }

    public Aspirant getAspirantFilter() {
        return aspirantFilter;
    }

    public void setAspirantFilter(Aspirant aspirantFilter) {
        this.aspirantFilter = aspirantFilter;
    }

    public void setAdminService(AdminService adminService) {
        this.adminService = adminService;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Inscribed getInscribedSelected() {
        return inscribedSelected;
    }

    public void setInscribedSelected(Inscribed inscribedSelected) {
        this.inscribedSelected = inscribedSelected;
    }

    public Round getRoundSelected() {
        return roundSelected;
    }

    public void setRoundSelected(Round roundSelected) {
        this.roundSelected = roundSelected;
    }

    public List<Inscribed> getInscribesSelected() {
        return inscribesSelected;
    }

    public void setInscribesSelected(List<Inscribed> inscribesSelected) {
        this.inscribesSelected = inscribesSelected;
    }
    
    
    public void loadAuctionSelected(Auction auctionSelected) {
         try {
            if (auctionSelected != null){
                    inscribedsByAction = inscribedService.findInscribedByAuction(auctionSelected);
                
                    winnersByAction = auctionService.findWinnersByAuction(auctionSelected);
                    auctionSelected.setWinners(winnersByAction);
                
                    productsByAction = auctionService.findAuctionProductByAuction(auctionSelected);
                    auctionSelected.setAuctionProducts(productsByAction);
                
                    roundsByAuction = roundService.findRoundsByAuction(auctionSelected);
                    auctionSelected.setRounds(roundsByAuction);
                    currentRound = roundService.loadCurrentRoundByAuction(auctionSelected);
                    loadTimeDefined(auctionSelected);
                    
            }    
        } 
        catch (AuctionInverseSystemException e) {
            e.printStackTrace();
        }
        catch (AuctionInverseValidationException e) {
            e.printStackTrace();
        }
    }
    public Auction getAuctionSelected() {
        return auctionSelected;
    }
    
    public void loadWinnersByRound () {
        try {
            if (roundSelected != null){
                winnersByRound = roundService.findWinnerByRound(roundSelected);
            }    
        } 
        catch (AuctionInverseSystemException e) {
            e.printStackTrace();
        }
        catch (AuctionInverseValidationException e) {
            e.printStackTrace();
        }
    }
    public List<Inscribed> getWinnersByRound () {
        return winnersByRound;
    }
    
    public void loadOfferByRoundInscribed(Round roundSelected, 
                            Inscribed inscribed, List<Offer> offersByRoundInscribed) {
        try {
            if (roundSelected != null){
                offersByRoundInscribed.addAll(roundService.
                        findOfferByRoundInscribed(roundSelected, inscribed));
                
            }    
        } 
        catch (AuctionInverseSystemException e) {
            e.printStackTrace();
        }
        catch (AuctionInverseValidationException e) {
            e.printStackTrace();
        }
    }
    
    public void loadOfferByRound(Round roundSelected, List<Offer> offersByRound) {
        try {
            if (roundSelected != null){
                offersByRound.addAll(roundService.findOffersByRound(roundSelected));
            }    
        } 
        catch (AuctionInverseSystemException e) {
            e.printStackTrace();
        }
        catch (AuctionInverseValidationException e) {
            e.printStackTrace();
        }
    }
    public List<Offer> getOffersByRound () {
        return offersByRound;
    }

    public void setAuctionSelected(Auction auctionSelected) {
        loadAuctionSelected(auctionSelected);
        this.auctionSelected = auctionSelected;
    }

    public Offer getOffer() {
        return offer;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }

    public List<Inscribed> getInscribedsByAction() {
        return inscribedsByAction;
    }

    public void setInscribedsByAction(List<Inscribed> inscribedsByAction) {
        this.inscribedsByAction = inscribedsByAction;
    }

    
    public boolean isAmin () {
         return "Admin".equals(getPersonLogged().getRol().getName());
    }
    
    public boolean isInsccribed () {
         return "Inscribed".equals(getPersonLogged().getRol().getName());
    }
    
    public boolean isAdminOrInsccribed () {
         return isAmin() || isInsccribed();
    }
    
    public boolean isAllowedStausChangeStatus() {
        if (auctionSelected != null) {
            return "CREATED".equals(auctionSelected.getStatus());
        }
        return false;
    }
    
    public boolean isAllowedOffer() {
        return isRoundInProgress() && isInsccribed() 
                && !"RETIRED".equals(getPersonLogged().getStatus()) 
                && !currentRound.getTimeTranscurred()
                        .equals(FormatterUtil.
                                formatterMinutesSecons(auctionSelected.getTimeForRound()));
    }
    
    public boolean isAllowedStartAuction() {
        return !isRoundInProgress() && isAmin();
    }
    
    public boolean isChatAllowed() {
        if (auctionSelected != null) {
            return ("OPEN".equals(auctionSelected.getStatus()) 
                    || "STARTED".equals(auctionSelected.getStatus())) && isAdminOrInsccribed();
        }
        return false;
    }
    
    public boolean isRoundInProgress() {
        if (currentRound != null) {
            return "INPROGRESS".equals(currentRound.getStatus()) 
                    && "STARTED".equals(auctionSelected.getStatus());
        }
        return false;
    }

    
    public List<Offer> getOffersSelected() {
        return offersSelected;
    }

    public void setOffersSelected(List<Offer> offersSelected) {
        this.offersSelected = offersSelected;
    }

    public Double getValueOffer() {
        return valueOffer;
    }

    public void setValueOffer(Double valueOffer) {
        this.valueOffer = valueOffer;
    }

    public String getOfferInScreen() {
        return offerInScreen;
    }

    public void setOfferInScreen(String offerInScreen) {
        this.offerInScreen = offerInScreen;
    }

    public Round getCurrentRound() {
        return currentRound;
    }

    public void setCurrentRound(Round currentRound) {
        this.currentRound = currentRound;
    }

    public String getTimeDefined() {
        return timeDefined;
    }

    public void setTimeDefined(String timeDefined) {
        this.timeDefined = timeDefined;
    }

    
}
