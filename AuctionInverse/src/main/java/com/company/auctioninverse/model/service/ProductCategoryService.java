

package com.company.auctioninverse.model.service;

import com.company.auctioninverse.model.businessobject.ProductCategory;

/**
 * Service that define contract for operations on product category entity.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
public interface ProductCategoryService extends GenericService<ProductCategory>{

}
