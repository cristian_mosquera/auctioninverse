

package com.company.auctioninverse.model.service;

import com.company.auctioninverse.model.businessobject.AuctionProduct;

/**
 * Service that define contract for operations on auction product entity.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
public interface AuctionProductService extends GenericService<AuctionProduct>{

}
