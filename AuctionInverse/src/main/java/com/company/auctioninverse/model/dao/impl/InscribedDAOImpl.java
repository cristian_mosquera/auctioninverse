package com.company.auctioninverse.model.dao.impl;

import com.company.auctioninverse.model.businessobject.Auction;
import com.company.auctioninverse.model.businessobject.Inscribed;
import com.company.auctioninverse.model.dao.InscribedDAO;
import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 * Class that implement contract for the inscribed operations  on SGBD. 
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
@Repository
public class InscribedDAOImpl extends GenericDAOImpl<Inscribed> implements InscribedDAO {

    @Override
    public List<Inscribed> findInscribedByAuction(Auction auction) 
                                                throws AuctionInverseSystemException {
        List parameters = new ArrayList();
        String sql = "select o from " + Inscribed.class.getSimpleName() + " o ";
            sql+= " left join fetch o.inscribedProducts inscribedProducts";
        if (auction != null) {
            sql += " where o.aspirant.auction.id = ?";
            parameters.add(auction.getId());
        }
        return getHibernateTemplate().find(sql, parameters.toArray());
    }
    
}
