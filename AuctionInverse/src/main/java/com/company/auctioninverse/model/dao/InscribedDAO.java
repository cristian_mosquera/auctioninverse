package com.company.auctioninverse.model.dao;

import com.company.auctioninverse.model.businessobject.Auction;
import com.company.auctioninverse.model.businessobject.Inscribed;
import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import java.util.List;

/**
 * Class that define contract for the inscribed operations on SGBD.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
public interface InscribedDAO extends GenericDAO<Inscribed> {
    
    /**
     * Method responsible of find inscribed by auction.
     * @param auction auction for filter.
     * @return list of inscribed by auction.
     * @throws AuctionInverseSystemException any system error
     */
    List <Inscribed> findInscribedByAuction(Auction auction) throws AuctionInverseSystemException;
}
