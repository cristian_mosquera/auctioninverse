package com.company.auctioninverse.model.dao.impl;

import com.company.auctioninverse.model.businessobject.Convenning;
import com.company.auctioninverse.model.dao.ConvenningDAO;
import org.springframework.stereotype.Repository;

/**
 * Class that implement contract for the convenning operations  on SGBD. 
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
@Repository
public class ConvenningDAOImpl extends GenericDAOImpl<Convenning> implements ConvenningDAO {
    
}
