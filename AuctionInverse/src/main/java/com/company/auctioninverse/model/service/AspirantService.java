

package com.company.auctioninverse.model.service;

import com.company.auctioninverse.model.businessobject.Aspirant;
import com.company.auctioninverse.model.businessobject.Auction;
import com.company.auctioninverse.model.exceptions.AuctionInverseSystemException;
import com.company.auctioninverse.model.exceptions.AuctionInverseValidationException;
import java.util.List;

/**
 * Service that define contract for operations on aspirant entity.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
public interface AspirantService extends GenericService<Aspirant>{

    /**
     * Method responsible of load aspirant by auction.
     * @param auction auction for filter
     * @return list of aspirants by auction.
     * @throws AuctionInverseSystemException any system error.
     * @throws AuctionInverseValidationException any validation error.
     */
    List<Aspirant> findAspirantsByAuction(Auction auction) 
            throws AuctionInverseSystemException, AuctionInverseValidationException;
    
    /**
     * Method responsible of load all aspirants by many auction.
     * @param auctions auctions to load.
     * @throws AuctionInverseSystemException any system error.
     * @throws AuctionInverseValidationException any validation error.
     */
    void loadAspirantsByAuctions(List<Auction> auctions) 
            throws AuctionInverseSystemException, AuctionInverseValidationException;
}
