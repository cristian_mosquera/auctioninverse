

package com.company.auctioninverse.model.service;

import com.company.auctioninverse.model.businessobject.Admin;

/**
 * Service that define contract for operations on admin entity.
 * @author <a href="mailto:mosquerapuello@gmail.com">Cristian Jose Mosquera Puello</a>
 */
public interface AdminService extends GenericService<Admin>{

}
